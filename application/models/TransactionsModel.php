<?php
        class TransactionsModel extends CI_Model {

                public $title;
                public $content;
                public $date;

                public function insertTransaction($total, $payment)
                {
                    $result = $this->db->query("SELECT COUNT(*) AS count FROM transaction");
                    $row = $result->row();
                    $trans_ID = $row->count;
                    $trans_ID++;

                    $result = $this->db->query("SELECT transaction_ID FROM transaction ORDER BY transaction_ID DESC LIMIT 1");
                    $trans_ID = $result->row('transaction_ID');
                    $trans_ID++;
                    $customer_ID = $_SESSION["accountID"];

                    $this->db->query("INSERT INTO transaction (transaction_ID ,transaction_Date, customer_ID, transaction_total, payment) 
                                      VALUES ({$trans_ID}, now(), {$customer_ID}, {$total}, '{$payment}')");
                    $this->db->query("INSERT INTO delivery (delivery_request_date, transaction_id)
                                  VALUES (now(), {$trans_ID})");

                    return $trans_ID;
                }
                
                public function insertLineitems($trans_ID, $prod_ID, $qty){
                    $this->db->query("INSERT INTO line_item (transaction_ID, product_ID, qty) 
                                      VALUES ({$trans_ID}, {$prod_ID}, {$qty})");
                }

                public function get_transanctionsdelivered($id){
                    $result = $this->db->query("SELECT * FROM transaction t 
                                                JOIN delivery d
                                                ON d.transaction_id = t.transaction_ID
                                                WHERE customer_ID = {$id} AND 
                                                      isDelivered = 1
                                                        ");
                    return $result->result();
                }
                public function get_transanctionsundelivered($id){
                    $result = $this->db->query("SELECT * FROM transaction t 
                                                JOIN delivery d
                                                ON d.transaction_id = t.transaction_ID
                                                WHERE customer_ID = {$id} AND 
                                                      isDelivered = 0
                                                        ");
                    return $result->result();
                }

                public function delete_transaction($id){
                    $this->db->query("DELETE d, li
                                                FROM delivery d
                                                JOIN line_item li
                                                ON li.transaction_ID = d.transaction_id
                                                WHERE li.transaction_ID = {$id}
                                    ");

                    $this->db->query("DELETE 
                                                FROM transaction
                                                WHERE transaction_ID = {$id}
                                    ");

                }

                public function get_all_transactions(){
                    $result = $this->db->query("SELECT * FROM transaction");
                    return $result->result();
                }

                public function get_line_items($id){
                    $result = $this->db->query("SELECT qty, product_name, product_price, product_price * qty as 'subtotal' 
                                                FROM line_item li
                                                JOIN product p
                                                on p.product_ID = li.product_ID
                                                WHERE li.transaction_ID = {$id}
                                                ");
                    return $result->result();
                }
                public function get_undelivered(){
                    $result = $this->db->query("SELECT d.delivered_by AS cour,t.transaction_ID AS tid,p.person_ID AS pid,transaction_total AS total , d.delivery_request_date AS req_date, d.delivery_finish as date_fin, t.payment AS pay, p.person_address AS address,p.person_lname AS lname, d.delivery_id AS did
FROM `delivery` d JOIN transaction t ON t.transaction_ID = d.transaction_id JOIN person p ON p.person_ID = t.customer_ID WHERE d.isDelivered = 0");
                    return $result->result();
                }
                public function get_delivered(){
                    $result = $this->db->query("SELECT d.delivered_by AS cour,t.transaction_ID AS tid,p.person_ID AS pid,t.transaction_total AS total ,d.delivery_id AS did,p.person_address AS address, d.delivery_request_date AS req_date, d.delivery_finish as date_fin, p.person_lname AS lname FROM `delivery` d LEFT JOIN transaction t ON t.transaction_ID = d.transaction_id JOIN person p ON p.person_ID = t.customer_ID WHERE d.isDelivered = 1");
                    return $result->result();
                }

                public function delivered($id,$courier){
                    $this->db->query("UPDATE `delivery` SET `delivery_finish`= NOW(),`isDelivered`= true,`delivered_by`={$courier} WHERE delivery_id = {$id}");
                    header("location : http://localhost/gamesbox/welcome/courier");
                }
                public function chart1data(){
                    $chart1_data  = $this->db->query("SELECT product_type,COUNT(*) AS x FROM line_item l JOIN product p ON p.product_ID = l.product_ID GROUP BY p.product_type ORDER BY x DESC");
                    return $chart1_data->result();
                }
                public function chart2data(){
                    $chart2_data  = $this->db->query("SELECT person_type,COUNT(*) AS x
                FROM person
                GROUP BY person_type
                ORDER BY x DESC");
                    return $chart2_data->result();
                }
                public function chart3data(){
                    $chart1_data  = $this->db->query("SELECT isActive,COUNT(*) AS x FROM person GROUP BY isActive");
                    return $chart1_data->result();
                }
                public function del_delivery($id){
                    $this->db->query("DELETE FROM `delivery` WHERE delivery_id = {$id}");
                    header("location : http://localhost/gamesbox/welcome/courier");
                }
                public function get_transactions_isdelivered(){
                    $res = $this->db->query("SELECT t.*, d.isDelivered FROM transaction t JOIN delivery d ON d.transaction_id = t.transaction_ID");
                    return $res->result();
                }
                
                public function get_unapproved_deliveries(){
                    $res = $this->db->query("SELECT t.transaction_ID AS tid, d.delivery_id AS did, p.person_ID AS pid, d.delivery_request_date AS req_date,t.payment AS payment, t.transaction_total AS total
FROM transaction t 
JOIN delivery d
ON d.transaction_id = t.transaction_ID 
JOIN line_item li 
ON t.transaction_ID=li.transaction_id 
JOIN person p 
ON t.customer_ID = p.person_ID 
WHERE d.employee_approved = false
GROUP BY t.transaction_ID
ORDER BY d.delivery_request_date ASC;");
                    return $res->result();
                }
               public function emp_approve($delivery_id){
                    $this->db->query("UPDATE delivery SET employee_approved = true WHERE delivery_id = {$delivery_id}");
                    header("location:http://localhost/gamesbox/welcome/employee_transactions");
                }
                public function emp_disapprove($delivery_id){

                   $transaction =  $this->db->query("SELECT transaction_ID FROM delivery WHERE delivery_id = {$delivery_id}");
                   $this->db->query("DELETE FROM delivery WHERE delivery_id = {$delivery_id}");
                   $this->db->query("DELETE FROM transaction WHERE transaction_ID = {$transaction}");
                   $this->db->query("DELETE FROM line_item WHERE transaction_ID = {$transaction}");
                }
        }   
