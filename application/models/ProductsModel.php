<?php
        class ProductsModel extends CI_Model {

                public $title;
                public $content;
                public $date;

                public function get_entries($type, $category)
                {
                    if($category == "All"){
                        $sql = "SELECT * FROM product";
                    }else if($category == "Video Games"){
                        $sql = "SELECT * FROM product WHERE product_type = 'Video Game'";
                    }else if($category == "Other"){
                        $sql = "SELECT * FROM product WHERE product_type = 'other'";
                    }else if($category == "Hardware"){
                        $sql = "SELECT * FROM product WHERE product_type = 'Hardware'";
                    }

                    if($type == "popular"){
                        $result = $this->db->query("SELECT * FROM ($sql)AS t");
                    }else if($type == "pricelotohi"){
                        $result = $this->db->query("SELECT * FROM ($sql)AS t ORDER BY product_price ASC");
                    }else if($type == "pricehitolo"){
                        $result = $this->db->query("SELECT * FROM ($sql)AS t ORDER BY product_price DESC");
                    }else{
                        echo "no sort type";
                    }
                        
                        return $result->result();
                }

                public function insert_entry($v)
                {
                       $data = array(
                                        'first_name' => $v['fname'],
                                        'username' => $v['uname'],
                                        'email' => $v['email'],
                                        'password' => $v['pword'],
                                        'active' => 1
                                );
              

                        $this->db->insert('account', $data);
                }



                public function update_entry()
                {
                        $this->title    = $_POST['title'];
                        $this->content  = $_POST['content'];
                        $this->date     = time();
                        $this->db->update('entries', $this, array('id' => $_POST['id']));
                }
                public function get_popular_products()
                {
                    $result = $this->db->query("SELECT COUNT(li.product_ID) AS count, p.*
                                                FROM line_item li
                                                JOIN product p
                                                ON li.product_ID =  p.product_ID
                                                GROUP BY li.product_ID
                                                ORDER BY count DESC
                                                LIMIT 8");
                    return $result->result();
                }
                public function get_new_products()
                {
                    $result = $this->db->query("SELECT * 
                                                FROM product
                                                ORDER BY product_ID DESC LIMIT 8");
                    return $result->result();
                }

                public function products_search($query)
                {
                    $result = $this->db->query("SELECT * FROM product WHERE product_name LIKE '%{$query}%' LIMIT 8");
                    return $result->result();
                }

                public function get_all_products()
                 {
                    $result = $this->db->query("SELECT * FROM product");
                    return $result->result();
                 }


                public function item_request($item)
                {
                    $result = $this->db->query("SELECT * FROM product WHERE product_ID = {$item}");
                    return $result->row();
                }
                public function add_product($name,$desc,$type,$price,$stock){
                    if(isset($_POST['product_name'])){
                        $this->db->query("INSERT INTO `product`(`product_ID`, `product_name`, `product_desc`, `product_price`, `product_type`, `product_stock`, `date_added`) VALUES ('','{$_POST['product_name']}','{$_POST['product_desc']}',{$_POST['product_price']},'{$_POST['product_type']}',{$_POST['product_stock']},NOW());");
                    }
                    header('location:http://localhost/gamesbox/welcome/employee_products');
                }
                public function remove_product($id){
                    $this->db->query("DELETE FROM `product` WHERE product_ID = {$id};");
                }
                public function add_stocks($id,$amount,$op){
                     $checkExisting = $this->db->query("SELECT * FROM product WHERE product_ID = {$id}");

                    if ($checkExisting->num_rows() == 0) {
                        $_SESSION['add_stock_error'] = "id";
                    }elseif(!is_numeric($amount) || $amount < 0){
                        $_SESSION['add_stock_error'] = "amount";
                    }else{
                        if($op == 'Add'){
                        $this->db->query("UPDATE product SET product_stock = product_stock + {$amount} WHERE product_ID = {$id}");
                        }else{
                        $this->db->query("UPDATE product SET product_stock = product_stock - {$amount} WHERE product_ID = {$id}");
                        }
                    
                    }
                }
}