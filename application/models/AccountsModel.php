    <?php
        class AccountsModel extends CI_Model {

                public $title;
                public $content;
                public $date;

                public function check_login($data)
                {
                    $_SESSION["isLoggedIn"] = false;
                    $_SESSION["email"] = "";

                    $result = $this->db->query("SELECT * FROM person WHERE person_email = '{$data['email']}' AND person_password ='{$data['password']}'");

                    if($result->num_rows() == 1){
                        $_SESSION["LoggedIn"] = true;
                        $_SESSION["loginerror"] = false;
                        $_SESSION["accountID"] = $result->row()->person_ID;
                        $_SESSION["email"] = $result->row()->person_email;
                        $_SESSION["fname"] = $result->row()->person_fname;
                        $_SESSION["lname"] = $result->row()->person_lname;
                       
                        $_SESSION["usertype"] = $result->row()->person_type;
                        redirect(base_url('welcome/index'));
                    }else{
                         $_SESSION["loginerror"] = true;
                         if(!empty($_SERVER['HTTP_REFERER'])){
                            redirect($_SERVER['HTTP_REFERER']);
                         }else {
                            // add here the default url for example dashboard url
                            redirect(base_url('welcome/index'));
                        }
                    }

                }

                public function check_password($pass)
                {
                    $retval;
                    $result = $this->db->query("SELECT * FROM person WHERE person_password ='{$pass}'");
                    if($result->num_rows() == 1){
                        $retval = true;
                        $_SESSION['passwordflag'] = false;
                    }else{
                        $retval = false;
                        $_SESSION['passwordflag'] = true;
                    }
                    return $retval;
                }

                public function get_account($id)
                {
                    $result = $this->db->query("SELECT * FROM person WHERE person_ID = {$id}");
                    return $result->row();
                }

                public function insert_account($data)
                {
                    $checkExisting = $this->db->query("SELECT * FROM person WHERE person_email = '{$data['email']}'");
                    if($checkExisting->num_rows() == 0){
                             $result = $this->db->query("INSERT INTO `person`(`person_fname`, `person_lname`, `person_phoneNum`, `person_address`, `person_type`, `person_password`, `person_email`, `isActive`, `date_joined`, city, zip_code) 
                                                VALUES('{$data['fname']}','{$data['lname']}', '{$data['contactnum']}','{$data['address']}','customer','{$data['password']}','{$data['email']}',true, now(), '{$data['city']}', {$data['zip_code']})"
                                              );
                    redirect(base_url('welcome/index'));
                }else{
                    $_SESSION['suError'] = true;
                    redirect(base_url('welcome/signup'));
                    }
                }

                public function update_personal($data)
                {
                    $this->db->query("UPDATE person 
                                    SET person_fname=\"{$data['fname']}\", person_lname=\"{$data['lname']}\",
                                        person_phoneNum=\"{$data['contactnum']}\", person_address=\"{$data['address']}\", city=\"{$data['city']}\", 
                                        zip_code={$data['zip_code']}
                                    WHERE person_ID = {$_SESSION['accountID']}
                                    ");
                    $_SESSION["fname"] = $data['fname'];
                    $_SESSION["lname"] = $data['lname'];
                    redirect(base_url('welcome/account'));

                }

                public function update_email($data)
                {
                    if($this->check_password($data['password'])){
                        $checkExisting = $this->db->query("SELECT * FROM person WHERE person_email = '{$data['email']}'");
                        if($checkExisting->num_rows() == 0){
                                $this->db->query("UPDATE person 
                                    SET person_email=\"{$data['email']}\"
                                    WHERE person_ID = {$_SESSION['accountID']}
                                    ");
                        }else{
                               $_SESSION['suError'] = true;
                         }

                    }
                    redirect(base_url('welcome/account'));
                }

                public function update_password($data)
                {
                    if($this->check_password($data['password'])){
                        $this->db->query("UPDATE person 
                                    SET person_password=\"{$data['newpass']}\"
                                    WHERE person_ID = {$_SESSION['accountID']}
                                    ");
                    }
                    redirect(base_url('welcome/account'));
                }
    
                public function get_all_active_customers()
                {
                    $result = $this->db->query("SELECT * FROM person WHERE person_type = 'customer' AND isActive = 1");
                    return $result->result();
                }
                public function get_all_inactive()
                {
                    $result = $this->db->query("SELECT * FROM person WHERE isActive = 0");
                    return $result->result();
                }
                public function get_all_employees(){
                     $result = $this->db->query("SELECT * FROM person WHERE person_type = 'employee' AND isActive = 1");
                     return $result->result();
                }
                public function get_all_couriers(){
                    $result = $this->db->query("SELECT * FROM person WHERE person_type = 'courier' AND isActive = 1");
                    return $result->result();
                }
                public function set_inactive($id){
                   
                        $this->db->query("UPDATE `person` SET `isActive`= 0 WHERE person_ID = {$id}");
                     }
                public function set_active($id){
                   
                        $this->db->query("UPDATE `person` SET `isActive`= 1 WHERE person_ID = {$id}");
                     }


                   
                
                // public function make_employee($id){
                //     $checkExisting = $this->db->query("SELECT * FROM person WHERE isActive = true AND person_ID = {$id} AND 
                //         (person_type = 'customer' OR person_type = 'courier')");
                //     if($checkExisting->num_rows() == 1){
                //         $this->db->query("UPDATE `person` SET `person_type`= '{$type}' WHERE person_id = {$id}");
                //     }else{
                //         $_SESSION['make_employee_error'] = true;
                //     }

                // }
                public function change_type($id,$type){
                        $checkExisting = $this->db->query("SELECT * FROM person WHERE person_ID = {$id}");
                        if($checkExisting->num_rows() == 1) {
                            $this->db->query("UPDATE `person` SET `person_type`= '{$type}' WHERE person_id = {$id}");
                        
                        }elseif($checkExisting->num_rows() == 0){
                            $_SESSION['IDerror'] = true;
                         }header("location:http://localhost/gamesbox/welcome/employee_customers");
                }


        }