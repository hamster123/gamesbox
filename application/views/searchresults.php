<div class="container topcomponent">
	<h3>Search Results - <span id="input"><?php echo $input?></span></h3>
	<hr>
	<div class="display">
		
	</div>
</div>
<!--cart modal-->
        <div class="modal fade" id="cartmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <div class="alert alert-warning modal-title w-100 modal-alert" role="alert">
                 
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <!--
              <div class="modal-body itemadded-body">
              </div>
                <div class="modal-footer">  
              </div>
              -->
            </div>
          </div>
        </div>

<script type="text/javascript">
$(document).ready(function(){
	Display($("#input").text());

	$(document).on("click", ".cartbtn", function(){
    var id = $(this).data('id');
    var name = $(this).data('name');
    var price = $(this).data('price');
    
    var base_url = "<?php echo base_url()?>";
    $.ajax({
        type: "POST",
        url : base_url +"welcome/cart_add",
        data: {
          id : id,
          name : name,
          price : price
        },
        dataType: "json",
        success: function(data) {
          $(".modal-alert").empty();
          $(".modal-alert").append(data.text);
        },
        error: function(data){
          alert("errorororor");
        }
     });
  });
	
});


function Display(query){
  var base_url = "<?php echo base_url()?>";
  $.ajax({
      url : base_url +"Welcome/products_search",
      method : "POST",
      data : {
        query : query
      },
      dataType : "json",
      success:function(data){
        if(!data.message){
          var numrows = data.length / 4;
          if(numrows % 1 !== 0){
            numrows = parseInt(numrows) + 1;
          }
          var i;
          var ctr;

          for(i=0, ctr=0; i != numrows; i++){
            var display_row = "<div class ='row card-deck'>";

            for(var ii=0; ii != 4 && ctr != data.length; ii++, ctr++){  
              display_row +="<div class='card cardclass col-3 mb-4 mx-2'>" +
                              "<div class='card-images mb-1'>" +
                                 "<a href=\"<?php echo base_url('welcome/item')?>/" + /*data[ctr].product_name*/ data[ctr].product_ID + "\">" +
                                 "<img class='card-img-top img-fluid' src='<?php echo base_url('resources/img/');?>" + data[ctr].product_ID + ".jpg'" + "alt='image not found'>" +
                                 "</a>" +
                              "</div>" +
                              "<div class='card-body'>" +
                                "<a href=\"<?php echo base_url('welcome/item')?>/" + /*data[ctr].product_name*/ data[ctr].product_ID + "\">" +
                                "<h5 class='card-title mb-1'>" + data[ctr].product_name + "</h5>" +
                                "</a>" +
                              "</div>" +
                              "<div class='card-footer bg-transparent'>" + 
                               "<button type='button' data-toggle='modal' data-target='#cartmodal' class='cartbtn btn btn-warning text-right my-0 mr-2' data-id='"+data[ctr].product_ID+"' data-name='"+data[ctr].product_name+"' data-price='"+data[ctr].product_price+"'>"+
                                "<span class='oi oi-cart' aria-hidden='true'></span>"+
                              "</button>       "+
                                "<span class='itemprice'>&#8369 " + data[ctr].product_price + "</span>" +
                              "</div>" +
                            "</div>";
            }
            display_row += "</div>";
            $(".display").append(display_row);
          }          
        }
      }
    });
}
</script>