
	    <link rel="stylesheet" type="text/css" href="<?php echo base_url('resources/css/dataTables.bootstrap.css');?>">
	    <link rel="stylesheet" type="text/css" href="<?php echo base_url('resources/css/datatables.min.css');?>">
	    <link rel="stylesheet" type="text/css" href="<?php echo base_url('resources/css/jquery.dataTables.css');?>">
	    <link rel="stylesheet" type="text/css" href="<?php echo base_url('resources/css/bootstrap.css');?>">
	    <link rel="stylesheet" type="text/css" href="<?php echo base_url('resources/css/bootstrap.min.css');?>">

		<style>
			.backdiv{
				height: 2em;
				width: 6em;
			}
			#backbutton,#modalbutton{
				margin-top: 5em;
				margin-left: 1em;
				margin-top: 1em;
				margin-bottom: 1em;
			}





		</style>
<div class="container">
		<div id="backdiv" style="margin-top: 5em;">

			<a href="<?php echo base_url('welcome/employee');?>"><button class="btn btn-warning" id="backbutton"><span class="oi oi-arrow-thick-left"></span> Back</button></a>

		</div>
		<div id="content mx-2">
			<div id="content-buttons"></div>
			<div id="content-table">
				<div class="display-table">
			<table class="display" id="pending_table">
				<thead> <h1 align="center">PENDING TRANSACTIONS</h1>
					<tr>
						<th>Transaction#</th>
						<th>Delivery#</th>
						<th>Customer# ID</th>
						<th>Date Requested</th>
						<th>Payment Option</th>
						<th>Transaction Total</th>
						<th>Items Ordered</th>
						<th>Approve?</th>
            
          
					</tr>
				</thead>
				<tbody>
					<?php
						$x = 0;
						while($x < count($unapproved)){
							echo "<tr align='center'>";
						 	echo "<td>{$unapproved[$x]->tid}</td>";
						 	echo "<td>{$unapproved[$x]->did}</td>";
							echo "<td>{$unapproved[$x]->pid}</td>";
							echo "<td>{$unapproved[$x]->req_date}</td>";
							echo "<td>{$unapproved[$x]->payment}</td>";
							echo "<td>{$unapproved[$x]->total}php</td>";
							echo "<td><button class='btn btn-warning' data-toggle='modal' data-target='#product_info_modal' id='{$unapproved[$x]->tid}' onClick='append_line_items(this.id)'><span class='oi oi-list'></span></button></td>";
							echo "<td>
								<button class='btn btn-success' id='{$unapproved[$x]->did}' onClick='approve(this.id)'><span class='oi oi-check' ></span></button>
								<button class='btn btn-danger' id='{$unapproved[$x]->did}' onClick='disapprove(this.id)'><span class = 'oi oi-x'></span></button>
							</td>";
							echo "</tr>";
							$x++;
						}
					?>
				</tbody>
			</table>
		</div>
			</div>
		</div>
		<div style="width: 100%;border:1px solid black; margin-top: 3em; margin-bottom: 1px"></div>
		<div style="width: 100%;border:1px solid black; margin-top: 0em; margin-bottom: 3em"></div>
			<div id="content mx-2">
			<div id="content-buttons"></div>
			<div id="content-table">
				<div class="display-table">
			<table class="display" id="completed_transactions">
				<thead> <h1 align="center">COMPLETED TRANSACTIONS</h1>
					<tr>
						<th>Transaction#</th>
						<th>Person#</th>
						<th>Date Requested</th>
						<th>Date Delivered</th>
						<th>Delivered By(Courier#)</th>
						<th>Total Payment</th>
						<th>Items Included</th>
						
						
            
          
					</tr>
				</thead>
				<tbody>
					<?php
						$x = 0;
						while($x < count($trans)){
							echo "<tr align='center'>";
						 	echo "<td>{$trans[$x]->tid}</td>";
						 	echo "<td>{$trans[$x]->pid}</td>";
							echo "<td>{$trans[$x]->req_date}</td>";
							echo "<td>{$trans[$x]->date_fin}</td>";
							echo "<td>{$trans[$x]->cour}</td>";
							echo "<td>{$trans[$x]->total}php</td>";
							echo "<td><button class='btn btn-warning' data-toggle='modal' data-target='#product_info_modal' id='{$trans[$x]->tid}' onClick='append_line_items(this.id)'><span class='oi oi-list'></span></button></td>";
							echo "</tr>";
							$x++;
						}
					?>
				</tbody>
			</table>
		</div>
			</div>
		</div>




				
   
</div>


<div id="product_info_modal" class="modal fade" role="dialog">
  <div class="modal-dialog">


    <div class="modal-content">
      <div class="modal-header"><h4>Products Included in this Order</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
          <!--enter products-->
          <table id="prodlist"><thead><tr>
          	<td>Product Name</td>
          	<td>Quantity</td>
          	<td>Price</td>
          	<td>Total</td></tr></thead>
          	<tbody id="appendhere">
          		
          	</tbody>
          	
          </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<script type="text/javascript">

	$(document).ready(function(){
		$('#completed_transactions').DataTable();    $('#pending_table').DataTable();

  }); 

      function set_delivered(clicked_id){
		var result = confirm("Are you sure?");
		if (result) {
		 var base_url = "<?php echo base_url()?>";
		    $.ajax({
		      url : base_url +"Welcome/set_delivery",
		      method : "POST",
		      data : {
		        id : clicked_id,
		        courier : <?php echo $_SESSION['accountID']; ?>
		      }
		  }); 
		    location.reload();
		}
	}
  	
	function approve(clicked_id){
var result = confirm("Are you sure?");
		if (result) {
		 var base_url = "<?php echo base_url()?>";
		    $.ajax({
		      url : base_url +"Welcome/set_approved",
		      method : "POST",
		      data : {
		        id : clicked_id,
		      }
		  }); 
		    location.reload();
		}
	}
	function disapprove(clicked_id){
var result = confirm("Are you sure?");
		if (result) {
		 var base_url = "<?php echo base_url()?>";
		    $.ajax({
		      url : base_url +"Welcome/set_disapproved",
		      method : "POST",
		      data : {
		        id : clicked_id,
		      }
		  }); 
		    location.reload();
		}
	}

	function append_line_items(clicked_id){
 var base_url = "<?php echo base_url()?>";
   $.ajax({
        type: "POST",
        url : base_url +"welcome/line_items",
        data: {
          id : clicked_id
        },
        dataType: "json",
        success: function(data) {
          console.log(data);
          for(var ctr=0; ctr != data.length; ctr++){

          	var row =	"<tr>";
          	row+=			"<td>"+data[ctr].product_name+"</td>";
          	row+=			"<td>"+data[ctr].qty+"</td>";
          	row+=			"<td>&#8369 "+data[ctr].product_price+"</td>";
          	row+=			"<td>&#8369 "+data[ctr].subtotal+"</td>";
          	row +=		"</tr>";
          	$("#appendhere").append(row);
          }
             $('#prodlist').DataTable();
        },
        error: function(data){
          alert("errorororor");
        }
     });
   }


</script>
