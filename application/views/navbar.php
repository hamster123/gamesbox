<!--NAVBAR-->
<div class="container-fluid fixed-top mb-4 flex-md-nowrap" id="navbarcolor">
  <nav class="navbar navbar-expand-sm navbar-light my-0 py-4">
    <a class="navbar-brand" href="<?php echo base_url('welcome/index');?>"><h4>GAMESBOX</h4></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item">
          <a class="nav-link" href="<?php echo base_url('welcome/index');?>">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo base_url('welcome/products');?>">Products</a>
        </li>
      
      </ul>

      <form class="mx-auto my-auto d-inline searchform" method="POST" action="<?php echo base_url('welcome/searchresults');?>">
            <div class="input-group">
              <input class="form-control form-control-sm border-right-0 w-50" id="search" type="text" placeholder="Search Products" aria-label="Search" list="results" name="input" required="required">
              <datalist id="results">
              </datalist>

              <div class="input-group-append">
                <button class="btn btn-warning border-left-0" type="submit"><span class="oi oi-magnifying-glass"></span> Search</button>
              </div>
            </div>

            <div>
            </div>
            
      </form>
      <!--
      <ul class="list-group">
                <li class="list-group-item">Cras justo odio</li>
                <li class="list-group-item">Dapibus ac facilisis in</li>
                <li class="list-group-item">Morbi leo risus</li>
                <li class="list-group-item">Porta ac consectetur ac</li>
                <li class="list-group-item">Vestibulum at eros</li>
      </ul>-->

      <ul class="navbar-nav flex-row">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class="oi oi-person mr-1"></span>
      <?php if(isset($_SESSION["LoggedIn"]) && $_SESSION["LoggedIn"] == true){
            $accountname = $_SESSION['fname'] ." ". $_SESSION['lname'];
      ?>
        <span id="accounttext"><?php echo $_SESSION["fname"];?></span>
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown"> 
            <!--
            <a class="dropdown-item" href="<?php echo base_url('welcome/login');?>">Log in</a>-->
            <a class="dropdown-item" href="<?php echo base_url('welcome/account');?>/<?php echo $accountname;?>">Account</a>
             <?php if($_SESSION['usertype'] == "employee"){?>
            <a class="dropdown-item" href="<?php echo base_url('welcome/employee');?>">Employee Page</a>
            <?php }elseif ($_SESSION['usertype'] == 'courier') { ?>
            <a class="dropdown-item" href="<?php echo base_url('welcome/courier');?>">Deliveries</a>
           <?php } ?>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="<?php echo base_url('welcome/logout');?>">Log Out</a>
          </div>
    <?php   
        }else{
    ?>
            <span id="accounttext"></span>
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
           <a class="dropdown-item" data-toggle="modal" data-target="#login" href="<?php echo base_url('welcome/login');?>">Log in</a>
            <a class="dropdown-item" href="<?php echo base_url('welcome/signup');?>">Sign Up</a>
          </div>
        
      <?php
          }
      ?>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo base_url('welcome/cart');?>"><span class="oi oi-cart mr-2"></span>Cart</a>
        </li>
      </ul>

    </div>
  </nav>
<!--
  <nav class="navbar navbar-expand-sm navbar-light my-0 navbar2nd">
    <div class="container d-flex justify-content-center">
        <input class="form-control mr-2" id="search" type="text" data-validation="alphanumeric length" data-validation-allowing="'" placeholder="Search" aria-label="Search" list="results">
        <button class="nav-item btn btn-warning my-2 my-sm-0" type="submit">Search</button>
        <datalist id="results">

        </datalist>
    </div>
  </nav>
-->
</div>

  <!----------------------------------------END OF TOP BAR------------------------------------------------------------------------------------>
<!-- Modal -->
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Log in Form</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container">
          <form method="POST" action="<?php echo base_url('welcome/logincheck');?>">
            <div class="form-group row">
              <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
              <div class="col-sm-10">
                <input type="email" class="form-control" id="inputEmail3" required=" required" data-validation="email" data-validation-length="max64" name="email" placeholder="Email">
              </div>
            </div>

            <div class="form-group row">
              <label for="inputPassword3" class="col-sm-2 col-form-label">Password</label>
              <div class="col-sm-10">
                <input type="password" required=" required" name="password" class="login-input form-control" id="inputPassword3" placeholder="Password">
              </div>
            </div>
            
            <div class="form-group row">
              <div class="offset-sm-2 col-sm-10">
                <button type="submit" class="btn btn-warning">Sign in</button>
              </div>
            </div>

          </form>

          <a href="<?php echo base_url('welcome/signup');?>">Create an Account</a>
        </div>
      </div>
    </div>
  </div>
</div>
<script>

$(document).ready(function(){
   var base_url = "<?php echo base_url()?>";
      
  //put in req_js later
  $("#search").on("keyup", function(){
    var input = $(this).val();
    $("#results").empty();
    $.ajax({
      url : base_url +"Welcome/products_search",
      method : "POST",
      data : {
        query : input
      },
      dataType : "json",
      success:function(data){
        if(!data.message){
          for(var i=0; i < data.length || i <= 5; i++){
            var option;
            option = "<a href=\"<?php echo base_url('welcome/item')?>/" + /*data[ctr].product_name*/ data[i].product_ID + "\">" +
                      "<option value=\""+data[i].product_name+"\">"  +
                     "</a>";
            $("#results").append(option);
          } 
        }
      }
    });
  });
});
</script>
