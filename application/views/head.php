<!DOCTYPE HTML>
<html>

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" type="text/css" href="<?php echo base_url('resources/css/product-display.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('resources/css/bootstrap.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('resources/css/open-iconic-bootstrap.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('resources/css/employee.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('resources/css/style.css');?>">

  <script src="<?php echo base_url('resources/js/jquery-3.3.1.min.js');?>"></script>
  <script src="<?php echo base_url('resources/js/bootstrap.min.js');?>"></script>
  <script src="<?php echo base_url('resources/js/jquery.form-validator.min.js');?>"></script>
  <script src="<?php echo base_url('resources/js/datatables.min.js');?>"></script>
  <script src="<?php echo base_url('resources/js/ourTable.js');?>"></script>
  <script src="<?php echo base_url('resources/js/highcharts.js');?>"></script>

</head>
<body>
  