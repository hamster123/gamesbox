
<div class="topcomponent container mr-5">
	<h2 class="mb-0">CART</h2>	
	<div class="row">
		<div class="col-md-8">
		<div align="right " class="mt-0 mb-1">
	    	<button type="button mt-0" id="clear_cart" class="btn btn-warning">Clear Cart</button>
	    </div>
		<table class="table table-bordered table-hover" id="navbarcolor">
		  <thead>
		    <tr>
		      <th>Product</th>
		      <th>Qty</th>
		      <th>Price</th>
		      <th>Subtotal</th>
		      <th class=""> Action</th>
		    </tr>
		  </thead>
		  <tbody>
		  	<?php $count = 0;
			foreach ($items as $values)
		    {  
		    	
		    	//echo $count;
		    	//echo $values['name'];
		    	$name = $values['name'];
		    	$rowid = $values['rowid'];
		    	$price = number_format($values['price'], 2);
		    	$subtotal = number_format($values['subtotal'], 2);
		    	echo "<tr id='{$rowid}' class='tbrow itemrow'>
		      			<th>{$name}</th>
		      			<td><input type='number' class='form-control qty' value='{$values['qty']}'></td>
		      			<td>&#8369 {$price}</td>
		      			<td id='subtotal'>&#8369 {$subtotal}</td>
		      			<td class=''><button type='button' name='remove' class='btn btn-danger remove btn-sm' id='{$rowid}'>Remove</button></td>
		    		</tr>";
		    }
			?>
			<tr>
				<!--
			    <td colspan="4" align="right">Total</td>
				<td id="total"></td>
				-->
			</tr>
		  </tbody>
		</table>
		
		</div>
		<div class="col-md-4 sticky-top" id="">
			<h4>Order Summary</h4>
			<hr>
			<span class="lead">Total: <span class="font-weight-bold" id="total">&#8369 <?php echo number_format($cart_total, 2);?></span> </span>
			<hr>
			<button type="button" class="btn btn-warning btn-lg w-100 m-auto" id="tocheckout">Proceed to CHECKOUT</button>
			<div class="collapse" id="collapsecheckout">
			   	<span class="">Delivery Address:</span><br>
			   	<span id="address"></span><br>
			   	<span id="city"></span><span id="zip_code"></span><br>

			   	<div class="form-check">
				  <input class="form-check-input" type="radio" name="paymentradio" id="ondelivery" value="option1" checked>
				  <label class="form-check-label" for="paymentradio1">
				    Pay on Delivery
				  </label>
				</div>
				<div class="form-check">
				  <input class="form-check-input" type="radio" name="paymentradio" id="withcard" value="option2">
				  <label class="form-check-label" for="paymentradio2">
				    Pay with Credit/Debit Card
				  </label>
				</div>
				<div class="collapse" id="collapsecard"><!--
					<form class="cardform">
						<div class="input-group mb-3">
							<div class="input-group-prepend">
								<label class="input-group-text" for="inputGroupSelect01">Choose Card</label>
							</div>
							<select class="custom-select" id="inputGroupSelect01">
							<option selected>VISA</option>
							<option value="1">Mastercard</option>
							</select>
						</div>
						<div class="form-group">
							<label for="cardnum" class="">Card number</label>
							<input type="text" name="ccard_num" class="form-control" id="cardnum" data-validation="creditcard" data-validation-allowing="visa" required="required"/>
						</div>
						<div class="form-group">
							<label for="cardnname" class="">Name on Card</label>
							<input type="text" class="form-control" id="cardname" required="required">
						</div>
						<div class="form-row">
							<br><label for="id" class="col-sm-4">Expiration Date</label>
							<input type="" class="form-control col-sm-4" id="mm" placeholder="mm" required=" required" data-validation="number">
							<input type="" class="form-control col-sm-4" id="yy" placeholder="yy" required=" required" data-validation="number">
						</div>
						<div class="form-group">
							<br><label for="id" class="">CCV / CVV</label>
							<input type="email" class="form-control" id="mm" data-validation="cvv" required=" required">
						</div>
					</form>-->
				</div>
				<button type="submit" class="btn btn-warning btn-lg w-100 m-auto" id="checkout" form=".cardform">CHECKOUT</button>
			   	
			</div>	
		</div>

	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){

 $(document).on('click', '.remove', function(){
  var row_id = $(this).attr("id");
  var tr = "#" + row_id;
  if(confirm("Are you sure you want to remove this?"))
  {
   $.ajax({
    url:"<?php echo base_url(); ?>welcome/cart_remove",
    method:"POST",
    data : {
    	row_id : row_id
    },
    dataType: "json	",
    success:function(data)
    {
     $("#total").text(data.total);
     $(tr).remove();
    }
   });
  }
  else
  {
   return false;
  }
 });

 $("#tocheckout").on('click', function(){
 	if($("#total").text() != "0.00"){
 		$(this).attr("data-toggle", "collapse");
 		$(this).attr("data-target", "#collapsecheckout");
 		<?php
 		if(isset($_SESSION["LoggedIn"]) && $_SESSION["LoggedIn"] == true){	
	  	echo '
	  		$.ajax({
			    url:"'.base_url().'welcome/get_address",
			    method:"POST",
			    dataType: "json",
			    success:function(data)
			    {
			    	
			     	$("#address").text(data.person_address);
			     	$("#city").text(data.city);
			     	$("#zip_code").text("  " +data.zip_code);
			    },
			    error:function(data)
			    {
			    	alert("error");
			    }
			   });';
	  	 
	  	}else{
	  		echo "alert('Log in to proceed to checkout');";
	  	}
	  	?>
 	}else{
 		$(this).attr("data-toggle", "");
 		$(this).attr("data-target", "");
 		alert("Cart is empty");
 	}
 });

$('#ondelivery').change(function() {

	 	$(this).attr("data-toggle", "collapse");
	 	$(this).attr("data-target", "#collapsecard");
});


 $('#withcard').change(function(){
 	$(this).attr("data-toggle", "collapse");
 	$(this).attr("data-target", "#collapsecard");

});

 $("#checkout").on('click', function(){
 	var payment;
 	if($('#ondelivery').is(':checked')){
 		payment = "on_delivery";
 	}else{
 		payment = "with_card";
 	}

 	var base_url = "<?php echo base_url()?>";
	  		$.ajax({
			    url: base_url+"welcome/transaction_add",
			    method:"POST",
			    data:{
			    	payment : payment
			    },
			    success:function(data)
			    {
			     	$(".itemrow").remove();
			     	$("#total").text("0.00");
			     	alert("Transaction Completed!");
			    },
			    error:function(data)
			    {
			    	alert("error");
			    }
			   });
 });

 $(".qty").bind('keyup mouseup', function () {
   var qty = $(this).val();
   var row_id = $(this).closest('tr').attr('id');
   var tr = "#" + row_id;
   var subtotal = $(this).parent().siblings("#subtotal").text();

   $.ajax({
    url:"<?php echo base_url(); ?>welcome/cart_updateqty",
    method:"POST",
    data : {
    	row_id : row_id,
    	qty : qty
    },
    dataType: "json",
    success:function(data)
    {
     	subtotal = data.subtotal;
     	//alert(subtotal);
     	$(tr).children("#subtotal").text(subtotal);
     	$("#total").text(data.total);
     	if(qty == 0){
     		$(tr).remove();
     	}
    },
    error:function(data)
    {
    	alert("error");
    }
   });

   
 });

 $(document).on('click', '#clear_cart', function(){
  if(confirm("Are you sure you want to clear cart?"))
  {
   $.ajax({
    url:"<?php echo base_url(); ?>welcome/cart_clear",
    success:function(data)
    {

    	$(".itemrow").remove();
    	$("#total").text("0.00");
    }
   });
  }
  else
  {
   return false;
  }
 });

});

</script>