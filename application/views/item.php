<?php 
$url = base_url('resources/img/');


?>

<div class="container topcomponent">
	<h2 class=""><?php echo $item->product_name?></h2>
	<!--
	<div>
    	<hr class="mb-0">
    </div>	-->
    <div class="row ">
		<div class="itemimg border-right p-3 mt-0 col-md-6 border-top">
		<?php echo "<img src=\"{$url}{$item->product_ID}.jpg\" class=\"img-fluid\">";?>
		</div>
		<div class="p-3 col-md-6 border-top">
			<span class="lead font-weight-bold">Product Description:</span>
			<p class="lead">
				<?php echo $item->product_desc?>
			</p>
			<br>
			<span class="lead font-weight-bold">Type:</span>
			<p class="lead">
				<?php echo $item->product_type?>
			</p>
			<h4> &#8369 <?php echo $item->product_price?></h4>
			<button type="button" class="btn btn-warning cartbtn" data-toggle='modal' data-target='#cartmodal' data-id="<?php echo $item->product_ID ?>" data-name="<?php echo $item->product_name?>" data-price="<?php echo $item->product_price?>">
				<span class='oi oi-cart' aria-hidden='true'></span>   ADD TO CART
			</button>
		</div>
	</div>
</div>


</div>

<!--cart modal-->
        <div class="modal fade" id="cartmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <div class="alert alert-warning modal-title w-100 modal-alert" role="alert">
                 
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <!--
              <div class="modal-body itemadded-body">
              </div>
                <div class="modal-footer">  
              </div>
              -->
            </div>
          </div>
        </div>

<script type="text/javascript">
$(document).ready(function(){

  $(document).on("click", ".cartbtn", function(){
    var id = $(this).data('id');
    var name = $(this).data('name');
    var price = $(this).data('price');
    
    var base_url = "<?php echo base_url()?>";
    $.ajax({
        type: "POST",
        url : base_url +"welcome/cart_add",
        data: {
          id : id,
          name : name,
          price : price
        },
        dataType: "json",
        success: function(data) {
          $(".modal-alert").empty();
          $(".modal-alert").append(data.text);
        },
        error: function(data){
          alert("errorororor");
        }
     });
  });

}); 
</script>

