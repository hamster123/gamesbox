
<!--start of display-->
  <div class="container-fluid topcomponent">
    <div class="row">

      <div class="sidebar col-sm-2 d-inline">
        <div class="sidebar-sticky text-center">
          <ul class="flex-column list-group">
            <button class="list-group-item list-group-item-action btn btn-warning">All</button>
            <button class="list-group-item list-group-item-action btn btn-warning">Video Games</button>
            <button class="list-group-item list-group-item-action btn btn-warning">Hardware</button>
            <button class="list-group-item list-group-item-action btn btn-warning">Other</button>
          </ul>
        </div>
      </div>

      <div class="container-fluid offset-sm-2 col-sm-10">
        <div class="navbar mb-2">
          <span class="navbar-brand H1 category" id="asd">Popular</span>
          <form class="form-inline">
          <p class="nav-item my-2 mr-6"></p>
          <div class="nav-item  dropdown ">
            <button class="btn btn-sm btn-light dropdown-toggle mr-4 sortbutton" type="button" id="dropdownMenuButton sortbutton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Sort By :
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" id="popular">Popularity</a>
              <a class="dropdown-item" id="pricelotohi">Price low to high</a>
              <a class="dropdown-item" id="pricehitolo">Price high to low</a>
            </div>
          </div>
          </form>
        </div>
          
        <div class="display">   
              
        </div>
        <!--cart modal-->
        <div class="modal fade" id="cartmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <div class="alert alert-warning modal-title w-100 modal-alert" role="alert">
                 
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <!--
              <div class="modal-body itemadded-body">
              </div>
                <div class="modal-footer">  
              </div>
              -->
            </div>
          </div>
        </div>

      </div> 

    </div>
  </div>
  
<script>

$(document).ready(function(){
  var category = "All";
  var type = $('#popular').attr('id');

  sortDisplay(type, category);

  $(document).on("click", ".cartbtn", function(){
    var id = $(this).data('id');
    var name = $(this).data('name');
    var price = $(this).data('price');
    var y = $(this).parents("");
    
    var base_url = "<?php echo base_url()?>";

    $.ajax({
        type: "POST",
        url : base_url +"welcome/cart_add",
        data: {
          id : id,
          name : name,
          price : price
        },
        dataType: "json",
        success: function(data) {
          $(".modal-alert").empty();
          $(".modal-alert").append(data.text);
        },
        error: function(data){
          alert("errorororor");
        }
     });
  });


  $(document).on("click", ".list-group-item", function(){
    category = $(this).text();
    $(".display").empty();
    sortDisplay(type, category);
  });

  $("#popular").on("click", function(){
    $(".display").empty();
    $("#asd").text("Popular");
    type = $(this).attr('id');
    //$("#sortbutton").val($("#featured").val());
    sortDisplay(type, category);
  });

  $("#pricelotohi").on("click", function(){
    //$("#sortbutton").text($("#pricelotohi").text());
    $("#asd").text("Low To High");
    $(".display").empty();
    type = $(this).attr('id');
    sortDisplay(type, category);
  });

  $("#pricehitolo").on("click", function(){
     $("#asd").text("High To Low");
    //$(".sortbutton").attr('value', "Price high to low");
    $(".display").empty();
    type = $(this).attr('id');
    sortDisplay(type, category);
  });
});

function sortDisplay(type, category){
  var base_url = "<?php echo base_url()?>";
  $.ajax({
      url : base_url +"Welcome/products_request",
      method : "POST",
      data : {
        category : category,
        type : type
      },
      dataType : "json",
      success:function(data){
        if(!data.message){
          var numrows = data.length / 4;
          if(numrows % 1 !== 0){
            numrows = parseInt(numrows) + 1;
          }
          var i;
          var ctr;

          for(i=0, ctr=0; i != numrows; i++){
            var display_row = "<div class ='row card-deck'>";

            for(var ii=0; ii != 4 && ctr != data.length; ii++, ctr++){  
              display_row +="<div class='card cardclass col-3 mb-4 mx-2'>" +
                              "<div class='card-images mb-1'>" +
                                 "<a href=\"<?php echo base_url('welcome/item')?>/" + /*data[ctr].product_name*/ data[ctr].product_ID + "\">" +
                                 "<img class='card-img-top img-fluid' src='<?php echo base_url('resources/img/');?>" + data[ctr].product_ID + ".jpg'" + "alt='image not found'>" +
                                 "</a>" +
                              "</div>" +
                              "<div class='card-body'>" +
                                "<a href=\"<?php echo base_url('welcome/item')?>/" + /*data[ctr].product_name*/ data[ctr].product_ID + "\">" +
                                "<h5 class='card-title mb-1'>" + data[ctr].product_name + "</h5>" +
                                "</a>" +
                              "</div>" +
                              "<div class='card-footer bg-transparent'>" + 
                               "<button type='button' data-toggle='modal' data-target='#cartmodal' class='cartbtn btn btn-warning text-right my-0 mr-2' data-id='"+data[ctr].product_ID+"' data-name='"+data[ctr].product_name+"' data-price='"+data[ctr].product_price+"'>"+
                                "<span class='oi oi-cart' aria-hidden='true'></span>"+
                              "</button>       "+
                                "<span class='itemprice'>&#8369 " + data[ctr].product_price + "</span>" +
                              "</div>" +
                            "</div>";
            }
            display_row += "</div>";
            $(".display").append(display_row);
          }          
        }
      }
    });
}



</script>



