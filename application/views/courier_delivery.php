
	    <link rel="stylesheet" type="text/css" href="<?php echo base_url('resources/css/dataTables.bootstrap.css');?>">
	    <link rel="stylesheet" type="text/css" href="<?php echo base_url('resources/css/datatables.min.css');?>">
	    <link rel="stylesheet" type="text/css" href="<?php echo base_url('resources/css/jquery.dataTables.css');?>">
	    <link rel="stylesheet" type="text/css" href="<?php echo base_url('resources/css/bootstrap.css');?>">
	    <link rel="stylesheet" type="text/css" href="<?php echo base_url('resources/css/bootstrap.min.css');?>">

		<style>
			.backdiv{
				height: 2em;
				width: 6em;
			}
			#backbutton,#modalbutton{
				margin-top: 5em;
				margin-left: 1em;
				margin-top: 1em;
				margin-bottom: 1em;
			}





		</style>
	<body>
<div class="container">		
		<div id="backdiv" style="margin-top: 5em;">
			<a href="<?php echo base_url('welcome/index');?>"><button class="btn btn-warning" id="backbutton"><span class="oi oi-arrow-thick-left"></span>  Back</button></a>
		</div>

		<div id="content mx-2">
			<div id="content-buttons"></div>
			<div id="content-table">
				<div class="display-table">
			<table class="display" id="unfinished_deliveries_table">
				<thead>Undelivered Transactions 
					<tr>
						<th>Delivery#</th>
						<th>Transaction#</th>
						<th>User ID</th>
						<th>Last Name</th>
						<th>Address</th>
						<th>Transaction Total</th>
						<th>Items Included</th>
			            <th></th>
			          	<th></th>
					</tr>
				</thead>
				<tbody>
					<?php
						$x = 0;
						while($x < count($undelivered)){
							echo "<tr align='center'>";
						  echo "<td>{$undelivered[$x]->did}</td>";
							echo "<td>{$undelivered[$x]->tid}</td>";
							echo "<td>{$undelivered[$x]->pid}</td>";
							echo "<td>{$undelivered[$x]->lname}</td>";
							echo "<td>{$undelivered[$x]->address}</td>";
							echo "<td>{$undelivered[$x]->total}</td>";
							echo "<td><button class='btn btn-warning' data-toggle='modal' data-target='#product_info_modal' id='{$undelivered[$x]->tid}' onClick='append_line_items(this.id)'><span class='oi oi-list'></span></button></td>";
							 echo "<td><button  onClick = 'set_delivered(this.id);' class='btn btn-success option' id='{$undelivered[$x]->did}' >Delivered</button></td>";
							 echo "<td><button  onClick = 'cancel_deliver(this.id);' class='btn btn-danger option' id='{$undelivered[$x]->did}' >Canceled</button></td>";
						
							echo "</tr>";
							$x++;
						}
					?>
				</tbody>
			</table>
		</div>
			</div>
		</div>
    <div id="content mx-2">
      <div id="content-buttons"></div>
      <div id="content-table">
        <div class="display-table">
      <table class="display" id="finished_deliveries_table">
        <thead>Undelivered Transactions 
          <tr>
            <th>Delivery#</th>
            <th>Transaction#</th>
            <th>User ID</th>
            <th>Last Name</th>
            <th>Address</th>
            <th>Transaction Total</th>
            <th>Delivered By (Courier#)</th>
            <th>Items Included</th>
          
          </tr>
        </thead>
        <tbody>
          <?php
            $x = 0;
            while($x < count($delivered)){
              echo "<tr align= 'center'>";
              echo "<td>{$delivered[$x]->did}</td>";
              echo "<td>{$delivered[$x]->tid}</td>";
              echo "<td>{$delivered[$x]->pid}</td>";
              echo "<td>{$delivered[$x]->lname}</td>";
              echo "<td>{$delivered[$x]->address}</td>";
              echo "<td>{$delivered[$x]->total}</td>";
              echo "<td>{$delivered[$x]->cour}</td>";
              echo "<td><button class='btn btn-warning' data-toggle='modal' data-target='#product_info_modal' id='{$delivered[$x]->tid}' onClick='append_line_items(this.id)'><span class='oi oi-list'></span></button></td>";
               
            
              echo "</tr>";
              $x++;
            }
          ?>
        </tbody>
      </table>
    </div>
      </div>
    </div>
    
</div>

<div id="product_info_modal" class="modal fade" role="dialog">
  <div class="modal-dialog">


    <div class="modal-content">
      <div class="modal-header"><h4>Products Included in this Order</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
          <!--enter products-->
          <table id="prodlist"><thead><tr>
          	<td>Product Name</td>
          	<td>Quantity</td>
          	<td>Price</td>
          	<td>Total</td></tr></thead>
          	<tbody id="appendhere">
          		
          	</tbody>
          	
          </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
	</body>
</html>



<script type="text/javascript">

	$(document).ready(function(){

    $('#unfinished_deliveries_table').DataTable();
 
    $('#finished_deliveries_table').DataTable();
  }); 

      function set_delivered(clicked_id){
var result = confirm("Are you sure?");
if (result) {
 var base_url = "<?php echo base_url()?>";
    $.ajax({
      url : base_url +"Welcome/set_delivery",
      method : "POST",
      data : {
        id : clicked_id,
        courier : <?php echo $_SESSION['accountID']; ?>
      }
  }); 
    location.reload();
}
}
  function cancel_deliver(clicked_id){
var result = confirm("Are you sure?");
if (result) {
 var base_url = "<?php echo base_url()?>";
    $.ajax({
      url : base_url +"Welcome/delete_delivery",
      method : "POST",
      data : {
        id : clicked_id,
      }
  }); 
    location.reload();
}
}
function append_line_items(clicked_id){
 var base_url = "<?php echo base_url()?>";
   $.ajax({
        type: "POST",
        url : base_url +"welcome/line_items",
        data: {
          id : clicked_id
        },
        dataType: "json",
        success: function(data) {
          console.log(data);
          for(var ctr=0; ctr != data.length; ctr++){

          	var row =	"<tr>";
          	row+=			"<td>"+data[ctr].product_name+"</td>";
          	row+=			"<td>"+data[ctr].qty+"</td>";
          	row+=			"<td>&#8369 "+data[ctr].product_price+"</td>";
          	row+=			"<td>&#8369 "+data[ctr].subtotal+"</td>";
          	row +=		"</tr>";
          	$("#appendhere").append(row);
          }
             $('#prodlist').DataTable();
        },
        error: function(data){
          alert("errorororor");
        }
     });
   }

  



</script>
