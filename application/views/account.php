
<div class="container topcomponent">
	<?php if(isset($_SESSION['accountID'])){?>
	<div>
		<h3>Account Information</h3>
		<div align="right " class="my-0">
	    	<button type="button" id="editbtn" class="btn btn-warning" data-toggle="collapse" data-target="#collapseconfirm">Edit</button>
	    </div>
		<hr class="mt-1">
		
		<form class="mt-0 mb-2" method="POST" action="<?php echo base_url('welcome/account_updatepersonal');?>"><!--
		  <div class="form-group row">
		    <label for="staticfname" class="col-sm-2 col-form-label">First Name</label>
		    <div class="col-sm-10">
		      <input type="text" readonly class="form-control" id="staticfname" value="<?php echo $_SESSION['fname']?>">
		    </div>
		  </div>
		  <div class="form-group row">
		    <label for="staticlname" class="col-sm-2 col-form-label">Last Name</label>
		    <div class="col-sm-10">
		      <input type="text" readonly class="form-control" id="staticlname" value="<?php echo $_SESSION['lname']?>">
		    </div>
		  </div>-->

		   <div class="form-row">
		    <div class="form-group col-md-6">
		      <label for="fname">First Name</label>
		      <input type="text" readonly class="form-control" id="fname" value="<?php echo $result->person_fname?>" name="fname" required="required" data-validation="alphanumeric" data-validation-length="max64">
		    </div>
		    <div class="form-group col-md-6">
		      <label for="lname">Last Name</label>
		      <input type="text" readonly class="form-control" id="lname" value="<?php echo $result->person_lname?>" name="lname" required="required" data-validation="alphanumeric" data-validation-length="max64">
		    </div>
		  </div>

		  <div class="form-group row">
		    <label for="email" class="col-sm-2 col-form-label border-right-0">Email</label>
		    <div class="col-sm-10">
		      <input type="text" readonly class="form-control" id="email" value="<?php echo $result->person_email?>">
		    </div>

		  </div>

		  <div class="form-group row">
		    <label for="contact" class="col-sm-2 col-form-label">Contact No.</label>
		    <div class="col-sm-10">
		      <input type="text" readonly class="form-control" id="contact" value="<?php echo $result->person_phoneNum?>" name="contactnum" required="required" data-validation="letternumeric" data-validation-allowing="-_" data-validation-length="max64">
		    </div>
		  </div>
		  <div class="form-group row">
		    <label for="address" class="col-sm-2 col-form-label">Address</label>
		    <div class="col-sm-10">
		      <input type="text" readonly class="form-control" id="address" value="<?php echo $result->person_address?>" name="address" required="required" data-validation="letternumeric" data-validation-length="max128" data-validation-allowing=" ,.-">
		    </div>
		  </div>
		  <div class="form-row">
			    <div class="form-group col-md-4">
			      <label for="inputCity">City</label>
			      <input type="text" value="<?php echo $result->city?>" required="required" data-validation="letternumeric" data-validation-length="max128" data-validation-allowing=" "
				class="form-control" placeholder="City" id="city" name="city" readonly>
			    </div>

			    <div class="form-group col-md-2">
			      <label for="inputZip">Zip</label>
			      <input type="text" value="<?php echo $result->zip_code?>" required="required" data-validation="number" class="form-control" id="zip_code" name="zip_code" readonly>
			    </div>
			</div>
		  <div class="collapse" id="collapseconfirm">
			  <div align="right " class="my-0">
		    	<button type="submit" id="confirmchange" class="btn btn-warning" >Confirm changes</button>
		    </div>
		  </div>
		</form>
		<button class="btn btn-warning mr-1" id="changeEmail" data-toggle="modal" data-target="#emailmodal">Change Email</button>
		<button class="btn btn-warning" id="changePass"  data-toggle="modal" data-target="#passwordmodal">Change Password</button>
	</div>
	<?php 
	}	
	?>
</div>

<!--Change Email Modal -->
<div class="modal fade" id="emailmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Change Email</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" action="<?php echo base_url('welcome/account_updateemail');?>">
		  <div class="form-group row">
		    <label for="staticEmail" class="col-sm-3 col-form-label">Current Email</label>
		    <div class="col-sm-9">
		      <input name="email" type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?php echo $result->person_email?>" data-validation="email" data-validation-length="max64">
		    </div>
		  </div>
		  <div class="form-group row">
		    <label for="inputPassword" class="col-sm-2 col-form-label">Password</label>
		    <div class="col-sm-10">
		      <input type="password" class="form-control" id="inputPassword" placeholder="Password" name='password'>
		    </div>
		  </div>
		  <div class="form-group row">
		    <label for="inputPassword" class="col-sm-2 col-form-label">New Email</label>
		    <div class="col-sm-10">
		      <input type="email" class="form-control" id="inputPassword" placeholder="Email" name='email'>
		    </div>
		  </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-warning">Save changes</button>
        </form>
      </div>

    </div>
  </div>
</div>

<!-- Change Password Modal -->
<div class="modal fade" id="passwordmodal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Change Password</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <form method="POST" action="<?php echo base_url('welcome/account_updatepassword');?>">
		  <div class="form-group">
		    <label for="exampleInputPassword1">Current Password</label>
		    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Current Password" name="password">
		  </div>
		  <div class="form-group">
		    <label for="exampleInputPassword1">New Password</label>
		    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="New Password" name="newpass" data-validation="length" data-validation-length="min8">
		  </div>
		  <div class="form-group">
		    <label for="exampleInputPassword1">Retype Password</label>
		    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="New Password" name="pass" data-validation="confirmation" data-validation-confirm="newpass">
		  </div>
      </div>

      <div class="modal-footer">
        <button type="submit" class="btn btn-warning">Save changes</button>
        </form>
      </div>

    </div>
  </div>
</div>

<div class="container">
	<hr>
	<h3>Transactions - completed</h3>
	<hr>
	<div class="displaytransactions">
		<?php foreach($transaction as $row){
			echo '<div class="card mb-1" id="navbarcolor">
					  <div class="card-body lead">
					  	<div class="row">
						    <span class="col-sm-3">Transaction #'.$row->transaction_ID.'</span>
						    <span class="col-sm-3">'.$row->transaction_Date.'</span>
						    <span class="col-sm-4">Total Amount:  &#8369 '.$row->transaction_total.'</span>
						    <span class="col-sm-2"><button class="btn btn-warning btn-sm expandbtn" id="expandbtn" data-toggle="collapse" data-target="#'.$row->transaction_ID.'" data-id="'.$row->transaction_ID.'">Expand</button></span>
					    </div>	
					  </div>
				 </div>
				 <div class="collapse" id="'.$row->transaction_ID.'">
				    <table class="table table-bordered table-hover" id="navbarcolor">
					  <thead>
					    <tr>
					      <th>Product</th>
					      <th>Qty</th>
					      <th>Price</th>
					      <th>Subtotal</th>
					    </tr>
					  </thead>
					  <tbody id="'.$row->transaction_ID.'display">
					  </tbody>
					</table>
				</div>';
		}
		?>
		
	</div>
</div>

<div class="container">
	<hr>
	<h3>Transactions - ongoing</h3>
	<hr>
	<div class="displaytransactions">
		<?php foreach($transactionun as $row){
			echo '<div class="card mb-1" id="navbarcolor">
					  <div class="card-body lead">
					  	<div class="row">
						    <span class="col-sm-3">Transaction #'.$row->transaction_ID.'</span>
						    <span class="col-sm-3">'.$row->transaction_Date.'</span>
						    <span class="col-sm-3">Total Amount:  &#8369 '.$row->transaction_total.'</span>
						    <span class="col-sm-1"><button class="btn btn-warning btn-sm expandbtn" id="expandbtn" data-toggle="collapse" data-target="#'.$row->transaction_ID.'" data-id="'.$row->transaction_ID.'">Expand</button></span>
						    <span class="col-sm-2"><button class="btn btn-danger btn-sm deletebtn" id="deletebtn" data-toggle="collapse" data-target="#'.$row->transaction_ID.'" data-id="'.$row->transaction_ID.'">Cancel Order</button></span>
					    </div>	
					  </div>
				 </div>
				 <div class="collapse" id="'.$row->transaction_ID.'">
				    <table class="table table-bordered table-hover ontable" id="navbarcolor">
					  <thead>
					    <tr>
					      <th>Product</th>
					      <th>Qty</th>
					      <th>Price</th>
					      <th>Subtotal</th>
					    </tr>
					  </thead>
					  <tbody id="'.$row->transaction_ID.'display">
					  </tbody>
					</table>
				</div>';
		}
		?>
		
	</div>
</div>


<script type="text/javascript">
$(document).ready(function(){
  
  $(document).on("click", "#editbtn", function(){
  	$(this).removeAttr('data-toggle');
  	$("#fname").attr("readonly", false);
  	$("#lname").attr("readonly", false); 
  	$("#contact").attr("readonly", false); 
  	$("#address").attr("readonly", false);
  	$("#city").attr("readonly", false);
  	$("#zip_code").attr("readonly", false);
  	$("#changePass").attr("disabled", true);
  	$("#changeEmail").attr("disabled", true);

  });

  $(document).on("click", "#confirmchange", function(){
  	$(this).removeAttr('data-toggle');
  	$("#fname").attr("readonly", true);
  	$("#lname").attr("readonly", true); 
  	$("#contact").attr("readonly", true); 
  	$("#address").attr("readonly", true);
  	$("#city").attr("readonly", true);
  	$("#zip_code").attr("readonly", true);
  	$("#changePass").attr("disabled", false);
  	$("#changeEmail").attr("disabled", false);

  });

  $(document).on("click", "#expandbtn", function(){
  	var tbody = "#";
  	tbody += $(this).data('id') + "display";
  	var id = $(this).data('id');
  	$(tbody).empty();
  	var base_url = "<?php echo base_url()?>";
  	
    $.ajax({
        type: "POST",
        url : base_url +"welcome/line_items",
        data: {
          id : id
        },
        dataType: "json",
        success: function(data) {
          console.log(data);
          for(var ctr=0; ctr != data.length; ctr++){

          	var row =	"<tr>";
          	row+=			"<td>"+data[ctr].product_name+"</td>";
          	row+=			"<td>"+data[ctr].qty+"</td>";
          	row+=			"<td>&#8369 "+data[ctr].product_price+"</td>";
          	row+=			"<td>&#8369 "+data[ctr].subtotal+"</td>";
          	row +=		"</tr>";
          	$(tbody).append(row);
          }
        },
        error: function(data){
          alert("errorororor");
        }
     });
  });

  $(document).on("click", "#deletebtn", function(){
  	if(confirm("Are you sure you want to cancel order?")){
  		var id = $(this).data('id');
  		var remove = "#";
  		//remove += id;
  		//remove += ontable;
	  	
		$(this).closest('.card').remove();
		$(this).closest('.ontable	').children('.ontable').remove();

	  	var base_url = "<?php echo base_url()?>";
	    $.ajax({
	        type: "POST",
	        url : base_url +"welcome/delete_transaction",
	        data: {
	          id : id
	        },
	        success: function(data) {
				
	        },
	        error: function(data){
	          alert("errorororor");
	        }
	     });
	}else{
		return false;
	}
  });


	<?php
   if(isset($_SESSION["passwordflag"])){
    if($_SESSION["passwordflag"]){
  	?>
  		alert("Incorrect Password!");
  <?php unset($_SESSION["passwordflag"]); }} ?>

  <?php
  if(isset($_SESSION['suError'])){
    if($_SESSION['suError']){ ?>
  		alert("Email already used!");
  <?php unset($_SESSION['suError']); }} ?>



});
</script>