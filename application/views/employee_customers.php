
	    <link rel="stylesheet" type="text/css" href="<?php echo base_url('resources/css/dataTables.bootstrap.css');?>">
	    <link rel="stylesheet" type="text/css" href="<?php echo base_url('resources/css/datatables.min.css');?>">
	    <link rel="stylesheet" type="text/css" href="<?php echo base_url('resources/css/jquery.dataTables.css');?>">
	    <link rel="stylesheet" type="text/css" href="<?php echo base_url('resources/css/bootstrap.css');?>">
	    <link rel="stylesheet" type="text/css" href="<?php echo base_url('resources/css/bootstrap.min.css');?>">

		<style>
			.backdiv{
				height: 2em;
				width: 6em;
			}
			#backbutton,#modalbutton{
				margin-top: 5em;
				margin-left: 1em;
				margin-top: 1em;
				margin-bottom: 1em;
			}



		</style>
<div class="container">
		<div id="backdiv" style="margin-top: 5em;">
			<a href="<?php echo base_url('welcome/employee');?>"><button class="btn btn-warning" id="backbutton"><span class="oi oi-arrow-thick-left"></span> Back</button></a>
			<button onClick = 'change_type(this.id);' class='btn btn-info option' id='{$customerlist[$x]->person_ID}' data-toggle='modal' data-target='#changetypemodal'>Change Type</button>
		</div>
		<div id="content mx-2">
			<div id="content-buttons"></div>
			<div id="content-table">
				<div class="display-table">
			<table class="display" id="customerstable">
				<thead><h1 style="margin-left: 40%">CUSTOMERS</h1>
					<tr>
						<th>ID</th>
						<th>Email</th>
						<th>First Name</th>
						<th>Last Name</th>
						<th>Address</th>
						<th>Phone Number</th>
            			<th></th>
					</tr>
				</thead>
				<tbody>
					<?php
						$x = 0;
						while($x < count($customerlist)){
							echo "<tr>";
						  echo "<td>{$customerlist[$x]->person_ID}</td>";
							echo "<td>{$customerlist[$x]->person_email}</td>";
							echo "<td>{$customerlist[$x]->person_fname}</td>";
							echo "<td>{$customerlist[$x]->person_lname}</td>";
							echo "<td>{$customerlist[$x]->person_address}</td>";
							echo "<td>{$customerlist[$x]->person_phoneNum}</td>";
							echo "<td><button onClick = 'make_inactive(this.id);' class='btn btn-danger option' id='{$customerlist[$x]->person_ID}'>Set as Inactive</button></td>";
							echo "</tr>";
							$x++;
						}
					?>
				</tbody>
			</table>
			<div style="border : 1px solid black; width: 100%;margin-top: 1em;"></div>
			<div style="border : 1px solid black; width: 100%;margin-top: 2px;"></div>
				</div>
			</div>
		</div>
		<div id="content-table" style="margin-top: 5em;">
				<div class="display-table">
			<table class="display" id="employeetable">
				<thead><h1 style="margin-left: 40%">EMPLOYEES</h1>
					<tr>
						<th>ID</th>
						<th>Email</th>
						<th>First Name</th>
						<th>Last Name</th>
						<th>Address</th>
						<th>Phone Number</th>
            			<th></th>
					</tr>
				</thead>
				<tbody>
					<?php
						$x = 0;
						while($x < count($employeelist)){
							echo "<tr>";
						  echo "<td>{$employeelist[$x]->person_ID}</td>";
							echo "<td>{$employeelist[$x]->person_email}</td>";
							echo "<td>{$employeelist[$x]->person_fname}</td>";
							echo "<td>{$employeelist[$x]->person_lname}</td>";
							echo "<td>{$employeelist[$x]->person_address}</td>";
							echo "<td>{$employeelist[$x]->person_phoneNum}</td>";
							echo "<td><button onClick = 'make_inactive(this.id);' class='btn btn-danger option' id='{$employeelist[$x]->person_ID}' >Set as Inactive</button></td>";
							echo "</tr>";
							$x++;
						}
					?>
				</tbody>
			</table>
			<div style="border : 1px solid black; width: 100%;margin-top: 1em;"></div>
			<div style="border : 1px solid black; width: 100%;margin-top: 2px;"></div>
			</div>
		</div>
</div>
<div class="container">
    <div id="content-table" style="margin-top: 5em;">
				<div class="display-table">
			<table class="display" id="courierTable">
				<thead><h1 style="margin-left: 44%">COURIERS</h1>
					<tr>
						<th>ID</th>
						<th>Email</th>
						<th>First Name</th>
						<th>Last Name</th>
						<th>Address</th>
						<th>Phone Number</th>
            			<th></th>
					</tr>
				</thead>
				<tbody>
					<?php
						$x = 0;
						while($x < count($couriers)){
							echo "<tr>";
						  echo "<td>{$couriers[$x]->person_ID}</td>";
							echo "<td>{$couriers[$x]->person_email}</td>";
							echo "<td>{$couriers[$x]->person_fname}</td>";
							echo "<td>{$couriers[$x]->person_lname}</td>";
							echo "<td>{$couriers[$x]->person_address}</td>";
							echo "<td>{$couriers[$x]->person_phoneNum}</td>";
							echo "<td><button onClick ='make_inactive(this.id);' class='btn btn-danger' id='{$couriers[$x]->person_ID}' >Set as Inactive</button></td>";
							echo "</tr>";
							$x++;
						}
					?>
				</tbody>
			</table>
			<div style="border : 1px solid black; width: 100%;margin-top: 1em;"></div>
			<div style="border : 1px solid black; width: 100%;margin-top: 2px;"></div>
			
				</div>
		</div>
</div>
<div class="container">
     <div id="content-table" style="margin-top: 5em;">
				<div class="display-table">
			<table class="display" id="inactiveTable">
				<thead><h1 style="margin-left: 35%">INACTIVE ACCOUNTS</h1>
					<tr>
						<th>ID</th>
						<th>Email</th>
						<th>First Name</th>
						<th>Last Name</th>
						<th>Address</th>
						<th>Type</th>
            			<th></th>
					</tr>
				</thead>
				<tbody>
					<?php
						$x = 0;
						while($x < count($inactive)){
							echo "<tr>";
						  echo "<td>{$inactive[$x]->person_ID}</td>";
							echo "<td>{$inactive[$x]->person_email}</td>";
							echo "<td>{$inactive[$x]->person_fname}</td>";
							echo "<td>{$inactive[$x]->person_lname}</td>";
							echo "<td>{$inactive[$x]->person_address}</td>";
							echo "<td>{$inactive[$x]->person_type}</td>";
							echo "<td><button onClick ='make_active(this.id);' class='btn btn-success' id='{$inactive[$x]->person_ID}' >Set as Active</button></td>";
							echo "</tr>";
							$x++;
						}
					?>
				</tbody>
			</table>
			<div style="border : 1px solid black; width: 100%;margin-top: 1em;"></div>
			<div style="border : 1px solid black; width: 100%;margin-top: 2px;"></div>
			
		</div>
			</div>
		</div>
</div>
    
<!-- change type modal -->
<div id="changetypemodal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">CHANGE USER TYPE
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
       		
       			 <form class="form-horizontal" method="POST" action="<?php echo base_url('Welcome/change_customer_type');?>">
        	<div>
        		<label for="uid">User ID:</label>
        		<input type="" name="uid" class="form-control" id="uid_input" >
        	</div>
        	
          <div class="form-control">
              <label for="product_stock">New Type:</label>
              <select name="type" class="form-control">
                <option>courier</option>
                <option>employee</option>
                <option>customer</option>
              </select>
            </div>
        	
        	<div><input type="submit" name="" class="btn btn-success" style="margin-top:1em;margin-left: 40%" ></div>

        </form>
       		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

	</body>
</html>
<script>
$('document').ready(function(){
	$('#customerstable').DataTable();
	$('#employeetable').DataTable();
	$('#courierTable').DataTable();
	$('#inactiveTable').DataTable();




});
	
	
</script>


<script type="text/javascript">
		function make_inactive(clicked_id){
var result = confirm("Set user with ID "+clicked_id+" as inactive?");
if (result) {
 var base_url = "<?php echo base_url()?>";
    $.ajax({
      url : base_url +"Welcome/make_inactive",
      method : "POST",
      data : {
        id : clicked_id,
        operation : 'inactive'
      }
  }); 
    location.reload();
}
}
function make_active(clicked_id){
var result = confirm("Set User with ID "+clicked_id+" as active?");
if (result) {
 var base_url = "<?php echo base_url()?>";
    $.ajax({
      url : base_url +"Welcome/make_active",
      method : "POST",
      data : {
        id : clicked_id,
        operation : 'active'
      }
  }); 
    location.reload();
}
}



	<?php if(isset($_SESSION['IDerror'])){?>
		alert('Invalid ID!');
	<?php unset($_SESSION['IDerror']); } ?>
</script>
