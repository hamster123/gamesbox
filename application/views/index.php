
<div class="container carouselcontainer topcomponent">
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100 img-fluid" src="<?php echo base_url('resources/img/witcher3.jpg');?>" alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100 img-fluid" src="<?php echo base_url('resources/img/pubg.jpg');?>" alt="Second slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100 img-fluid" src="<?php echo base_url('resources/img/sampleimg.jpg');?>" alt="Third slide">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
</div>
<!--end of carousel-->
<!--Popular Products-->

<!--FETCHING POPULAR PRODUCTS-->
<!--cart modal-->
        <div class="modal fade" id="cartmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <div class="alert alert-warning modal-title w-100 modal-alert" role="alert">
                 
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <!--
              <div class="modal-body itemadded-body">
              </div>
                <div class="modal-footer">  
              </div>
              -->
            </div>
          </div>
        </div>

<!--/-->
<div class="container">
  <div>
  <hr>
  </div>
  <div class="container-header"><h2>POPULAR PRODUCTS</h2></div>
  
  <div class="popular">

  </div> 
 
</div>




<div class="container">
    <div>
    <hr>
    </div>
    <div class="container-header"><h2>NEW PRODUCTS</h2></div>
    
    <div class="new">
    </div>
</div>


<script type="text/javascript">
$(document).ready(function(){
  displayproducts($(".popular").attr('class'));
  displayproducts($(".new").attr('class'));


  
  $(document).on("click", ".cartbtn", function(){
    var id = $(this).data('id');
    var name = $(this).data('name');
    var price = $(this).data('price');
    
    var base_url = "<?php echo base_url()?>";
    $.ajax({
        type: "POST",
        url : base_url +"welcome/cart_add",
        data: {
          id : id,
          name : name,
          price : price
        },
        dataType: "json",
        success: function(data) {
          $(".modal-alert").empty();
          $(".modal-alert").append(data.text);
        },
        error: function(data){
          alert("errorororor");
        }
     });
  });

}); 

  function displayproducts(type){
  var base_url = "<?php echo base_url()?>";
  $.ajax({
      url : base_url +"Welcome/products_display",
      method : "POST",
      data : {
        type : type
      },
      dataType : "json",
      success:function(data){
        if(!data.message){
          var i;
          var ctr;


          for(i=0, ctr=0; i != 2; i++){
            var display_row = "<div class ='row card-deck'>";

            for(var ii=0; ii != 4 && ctr != data.length; ii++, ctr++){  
              display_row +="<div class='card cardclass col-3 mb-4 mx-2'>" +
                              "<div class='card-images mb-1'>" +
                                 "<a href=\"<?php echo base_url('welcome/item')?>/" + /*data[ctr].product_name*/ data[ctr].product_ID + "\">" +
                                 "<img class='card-img-top img-fluid' src='<?php echo base_url('resources/img/');?>" + data[ctr].product_ID + ".jpg'" + "alt='image not found'>" +
                                 "</a>" +
                              "</div>" +
                              "<div class='card-body'>" +
                                "<a href=\"<?php echo base_url('welcome/item')?>/" + /*data[ctr].product_name*/ data[ctr].product_ID + "\">" +
                                "<h5 class='card-title mb-1'>" + data[ctr].product_name + "</h5>" +
                                "</a>" +
                                //"<p class='card-text mb-0'>" + data[ctr].product_desc + "</p>" +
                              "</div>" +
                              "<div class='card-footer bg-transparent'>" + 
                               //"<a href='#' class='btn btn-warning text-right my-0 mr-2'><span class='oi oi-cart' aria-hidden='true'></span></a>" +
                               "<button type='button' data-toggle='modal' data-target='#cartmodal' class='cartbtn btn btn-warning text-right my-0 mr-2' data-id='"+data[ctr].product_ID+"' data-name='"+data[ctr].product_name+"' data-price='"+data[ctr].product_price+"'>"+
                               //"<button type='button' class='cartbtn btn btn-warning text-right my-0 mr-2' id='"+data[ctr].product_ID+"'>"+
                                "<span class='oi oi-cart' aria-hidden='true'></span>"+
                              "</button>       "+
                                "<span class='itemprice'>" + data[ctr].product_price + " php" + "</span>" +
                              "</div>" +
                            "</div>";
            }
            display_row += "</div>";
            $("."+type).append(display_row);

          }   
             
        }
      }
    });
}

</script>
