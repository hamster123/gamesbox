<div class="topcomponent">
      <div class="sidebar col-sm-2 d-inline" style="margin-top: 3em;">
        <div class="sidebar-sticky text-center">
          <ul class="nav flex-column list-group">

      
            <li><a  href="<?php echo base_url('welcome/employee_products');?>"><button style="width: 100%;height: 100%;margin-top: 1em;" class="btn btn-warning" id="editproducts">Products</button></li></a>
            <li><a href="<?php echo base_url('welcome/employee_customers');?>"><button style="width: 100%;height: 100%;margin-top: 1em;" class="btn btn-warning" id="editcustomers">Users</button></li></a>
              <li><a href="<?php echo base_url('welcome/employee_transactions');?>"><button style="width: 100%;height: 100%;margin-top: 1em;" class="btn btn-warning" id="editcustomers">Transactions</button></li></a>

          </ul>

        </div>
      </div>
      <div class="col-md-8 col-md-offset-3" id="appendContainer" style="margin-left: 30%; margin-top : 3em;">
        <h3>CHARTS</h3>
        <hr>
        <div class="container p-4">
          <div id="one" class="chart"></div>
        </div>
        <hr>
        <div class="container p-4">
        <div id="two" class="chart"></div>
        </div>
        <hr>
        <div class="container p-4">
        <div id="thr" class="chart"></div>
        </div>
        <hr>
      </div>
</div>

<script src="<?php echo base_url('resources/js/jquery.min.js');?>"></script>
<script src="<?php echo base_url('resources/js/highcharts.js');?>"></script>
  <script>
     Highcharts.setOptions({
     colors: ['#2434d8',
          '#101766',
          '#87744d',
          '#6d5e3f',
          '#4f442d',
          '#6b5831',
          '#755f31']
  });
    
  var chart;
  
  $(function () {
    chart = Highcharts.chart('one', {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
    },
    title: {
      text: 'Percentage of all ordered products per transaction'
    },
    tooltip: {
      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: false
        },
        showInLegend: true
      }
    },
    series: [{
      name: 'Product Types',
      colorByPoint: true,
      data: [
          <?php
          $x = 0;
            while($x < 3){
              echo "['{$chart1[$x]->product_type}',{$chart1[$x]->x}],";
              $x++;
            }
          ?>
           ]
        }]
    });
  });
  




    
  var chart;
  
  //second chart
  $(function () {
    chart = Highcharts.chart('two', {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
    },
    title: {
      text: 'Composition of all persons'
    },
    subtitle: {
      text: 'per person type'
    },
    tooltip: {
      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: false
        },
        showInLegend: true
      }
    },
    series: [{
      name: 'Percentage',
      colorByPoint: true,
      data: [
          <?php
             $x = 0;
            while($x < count($chart2)){
              echo "['{$chart2[$x]->person_type}',{$chart2[$x]->x}],";
              $x++;
            }
            
          ?>
           ]
        }]
    });
  });
  
  var chart;
  
  //second chart
  $(function () {
    chart = Highcharts.chart('thr', {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
    },
    title: {
      text: 'Active and Inactive Users'
    },
    subtitle: {
      text: 'per person type'
    },
    tooltip: {
      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: false
        },
        showInLegend: true
      }
    },
    series: [{
      name: 'Percentage',
      colorByPoint: true,
      data: [
          <?php
             $x = 0;
            while($x < 2){
              echo "['{$chart3[$x]->isActive}',{$chart3[$x]->x}],";
              $x++;
            }
            
          ?>
           ]
        }]
    });
  });
  


  //   Highcharts.setOptions({
  //    colors: ['#6d5e3f',
  //         '#4f442d',
  //         '#6b5831',
  //         '#755f31']
  // });
    
  // var chart;
  
  // //third chart
  // $(function () {
  //   chart = Highcharts.chart('three', {
  //       chart: {
  //         type: 'column'
  //       },
  //       title: {
  //         text: 'Top 7 Customers with highest average subtotals'
  //       },
  //       subtitle: {
  //         text: 'per transaction'
  //       },
  //       xAxis: {
  //         categories: [
  //           <?php
  //             $y=0;
  //             while($y = count($chart3)){
  //               echo "{$chart3[$y]->x},";
  //             }
  //           ?>
  //         ],
  //         crosshair: true
  //       },
  //       yAxis: {
  //         min: 0,
  //         title: {
  //           text: 'Rainfall (mm)'
  //         }
  //       },
  //       tooltip: {
  //         headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
  //         pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
  //           '<td style="padding:0"><b>{point.y:.1f}Php</b></td></tr>',
  //         footerFormat: '</table>',
  //         shared: true,
  //         useHTML: true
  //       },
  //       plotOptions: {
  //         column: {
  //           pointPadding: 0.2,
  //           borderWidth: 0
  //         }
  //       },
  //       series: [{
  //         name: 'Customer IDs',
  //         data: [<?php
  //             $x=0;
  //             while($x = count($chart3)){
  //               echo "{$chart3[$x]->customer_ID},";
  //             }
  //           ?>]

  //       }]
  //     });
  // });
  



  // var x = 0;
  //   $(document).ready(function(){
  //     $('#storecharts').click(function(){
          
  //         if(x == 0){
  //           $('#appendContainer').append("<button id ='c1' class = 'chartbutton btn btn-info'>Chart1</button><button id='c2'  class = 'chartbutton  btn btn-info'>Chart2</button><button id = 'c3'  class = 'chartbutton btn btn-info'>Chart3</button>");
  //         }
        
  //       x++;
  //     });


  //   });
  </script>
