	
	<!--------------------------------------------------------END OF TOP BAR------------------------------------------------------------------------>
	<div class="container form-group topcomponent">
	<h1 id="logregmsg">Create a new account</h1>
		<form action="<?php echo base_url('welcome/signup_send');?>" method="POST" id="signupform" class="">
			<div id="inputdiv">
				<label for="email">Email:</label>
				<input type="text" required=" required" data-validation="email" data-validation-length="max64" name="email" class="form-control" placeholder="eg. email@sample.com">
			</div>

			<div id="inputdiv">
				<label for="password">Password:</label>
				<input type="password" name="password" required="required"  data-validation="length" data-validation-length="min8" class="form-control" placeholder="password">
			</div>

			<div class="form-group inputdiv">
		    <label for="exampleInputPassword1">Confirm Password</label>
		    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="password" name="pass" data-validation="confirmation" data-validation-confirm="password">
		  	</div>

			<div id="inputdiv">
				<label for="fname">First Name:</label>
				<input type="text" name="fname" class="form-control" required="required" data-validation="alphanumeric" data-validation-length="max64" placeholder="">
			</div>

			<div id="inputdiv">
			<label for="lname">Last Name:</label>
				<input type="text" name="lname" required="required" data-validation="alphanumeric" data-validation-length="max64" class="form-control" placeholder="">
			</div>

			<div id="inputdiv">
				<label for="address">Address:</label>
				<input type="text" name="address" required="required" data-validation="letternumeric" data-validation-length="max128" data-validation-allowing=" ,.-"
				class="form-control" placeholder="[House number] [Building] [Street Name] ">
			</div>

			<div class="form-row">
			    <div class="form-group col-md-6">
			      <label for="inputCity">City</label>
			      <input type="text" required="required" data-validation="letternumeric" data-validation-length="max128" data-validation-allowing=" "
				class="form-control" placeholder="City" id="inputCity" name="city">
			    </div>

			    <div class="form-group col-md-2">
			      <label for="inputZip">Zip</label>
			      <input type="text" required="required" data-validation="number" class="form-control" id="inputZip" name="zip_code">
			    </div>
			</div>

			<div id="inputdiv">
				<label>Contact Number:</label>
				<input type="text" required="required" data-validation="letternumeric" data-validation-allowing="-_" name="contactnum" data-validation-length="max64" class="form-control" placeholder="">
			</div>
			<div id="signup-submit-div" class="">
				<input type="submit" name="" class="btn btn-warning" style="margin-top: 3%">
			</div>
			
		</form>
	</div>




	<!------------------------------------------------------FOOTER------------------------------------------------------------------------>

<script>
 $.validate();
 $(document).ready(function(){
  <?php
  if(isset($_SESSION['suError'])){
    if($_SESSION['suError']){ ?>
  alert("Email already used!");
  <?php unset($_SESSION['suError']); }} ?>
});
</script>