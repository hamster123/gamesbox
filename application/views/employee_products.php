
      <link rel="stylesheet" type="text/css" href="<?php echo base_url('resources/css/dataTables.bootstrap.css');?>">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url('resources/css/datatables.min.css');?>">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url('resources/css/jquery.dataTables.css');?>">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url('resources/css/bootstrap.css');?>">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url('resources/css/bootstrap.min.css');?>">

		<style>
			.backdiv{
				height: 2em;
				width: 6em;
			}
			#backbutton,#modalbutton{
				margin-top: 5em;
				margin-left: 1em;
				margin-top: 1em;
				margin-bottom: 1em;
			}
      #addproductmodal{
        max-width: 50%;
      }





		</style>
<div class="container">
		<div id="backdiv" style="margin-top: 5em;">
			<a href="<?php echo base_url('welcome/employee');?>"><button class="btn btn-warning" id="backbutton"><span class="oi oi-arrow-thick-left"></span> Back</button></a>
			<button data-toggle="modal" data-target="#addproductModal" id="modalbutton" class="btn btn-info" type="button">+ ADD PRODUCT</button>
		</div>
		<div id="content mx-2">
			<div id="content-buttons"></div>
			<div id="content-table">
				<div class="display-table">
			<table class="display" id="productstable">
				<thead>Products 
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Type</th>
						<th>Amount in stock</th>
            <th></th>
            <th></th>
					</tr>
				</thead>
				<tbody>
					<?php
						$x = 0;
						while($x < count($productlist)){
							echo "<tr>";
						  echo "<td>{$productlist[$x]->product_ID}</td>";
							echo "<td>{$productlist[$x]->product_name}</td>";
							echo "<td>{$productlist[$x]->product_desc}</td>";
							echo "<td>{$productlist[$x]->product_price}</td>";
							echo "<td>{$productlist[$x]->product_type}</td>";
							echo "<td>{$productlist[$x]->product_stock}</td>";
							 echo "<td><button class='btn btn-danger option' id='{$productlist[$x]->product_ID}' onClick='product_remove(this.id)'>X</button></td>";
							echo "<td><button onClick = 'product_add_stock(this.id);' class='btn btn-success option' id='{$productlist[$x]->product_ID}' onClick='' data-toggle='modal' data-target='#add_stock_modal'>+/- Stock</button></td>";
							echo "</tr>";
							$x++;
						}
					?>
				</tbody>
			</table>
		</div>
			</div>
		</div>
    
	<div id="addproductModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
</div>
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">ADD A PRODUCT
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <div class="container">
        	<form method="POST" action="<?php echo base_url('welcome/insert_product');?>">
      			<div class="form-control">
      				<label for="product_name">Product Name: </label>
      				<input type="" name="product_name" class="form-control">
      			</div>
      			<div class="form-control">
      				<label for="product_desc">Description:</label>
      				<textarea name="product_desc" rows="2" cols="40" class="form-control"></textarea>
      			</div>
      			<div class="form-group">
      				<label for="product_type">Type:</label>
      				<select  name="product_type" class="form-control">
      					<option>Hardware</option>
      					<option>Video Game</option>
      					<option>other</option>
      				</select>
      			</div>
      			<div class="form-control">
      				<label for="product_price">Price:</label>
      				<input type="" name="product_price" class="form-control">
      			</div>
      			<div class="form-control">
      				<label for="product_stock">Amount to add</label>
      				<input type="" name="product_stock" class="form-control">
      			</div>
            <div>
              
            </div>
      			<input type="submit" class="btn btn-success" name="" style="margin-top: 1em;">
      		</form>
        </div>
      		

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<!--    ADD PRODUCT STOCK MODAL    -->
<div id="add_stock_modal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">ADD PRODUCT STOCK
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form class="form" method="POST" action="<?php echo base_url('welcome/add_stocks');?>">
        	<div class="row">
        		<label for="pID" class="col-sm-3">Product ID:</label>
        		<input type="text" name="pID" class="form-control-plaintext col-sm-9" readonly id="pid_input" >
        	</div>
        	<label for="pID">Amount:</label>
        	<div><input type="" name="amount" class="form-control"></div>
          <div class="form-control">
              <label for="product_stock">Operation:</label>
              <select name="operation" class="form-control">
                <option>Add</option>
                <option>Subtract</option>
              </select>
            </div>
        	
        	<div><input type="submit" name="" class="btn btn-success" style="margin-top:1em;margin-left: 40%" ></div>

        </form>
      </div> 
    </div>

  </div>
</div>
	</body>
</html>


<script>
	 // $("#productstable").on('click','.btn-danger',function(){
  //      $(this).closest('tr').remove();
  //    });
     $("#productstable").on('click','.btn-success',function(){
       document.getElementById('pid_input').value = this.id; 
     });

</script>


<script type="text/javascript">

	$(document).ready(function(){
    $('#productstable').DataTable();
<?php if(isset($_SESSION['add_stock_error'])){
  if($_SESSION['add_stock_error'] == 'id'){?>
        setTimeout(function() { alert("Invalid Product ID!"); }, 800);
    unset($_SESSION['add_stock_error']);
  <?php }elseif($_SESSION['add_stock_error'] == 'amount'){ ?>

    setTimeout(function() { alert("Invalid Amount!"); }, 800);
    unset($_SESSION['add_stock_error']);
<?php } 
$this->session->unset_userdata('add_stock_error');
} ?>


  });


	 function product_remove(clicked_id){
var result = confirm("Are you sure you want to delete Product#"+clicked_id+"?");
if (result) {
 var base_url = "<?php echo base_url()?>";
    $.ajax({
      url : base_url +"Welcome/delete_product",
      method : "POST",
      data : {
        id : clicked_id

      }
  }); 
    location.reload();
}
}

</script>
