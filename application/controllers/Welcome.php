<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->model("ProductsModel");
		$data['popular'] = $this->ProductsModel->get_popular_products();
		$data['new'] = $this->ProductsModel->get_new_products();
		$this->load->view('head');
		$this->load->view('navbar');
		$this->load->view('index', $data);
		$this->load->view('footer');
		$this->load->view('req_js');
		
	}

	public function products()
	{
		$this->load->view('head');
		$this->load->view('navbar');
		$this->load->view('products');
		$this->load->view('footer');
		$this->load->view('req_js');
	}

	public function item()
	{
		//$data['item'] = $this->uri->segment(3);
		//$data['item'] = str_replace("%20", " ", $data['item']);
		$item = $this->uri->segment(3);
		$this->load->model('ProductsModel');
		$result['item'] = $this->ProductsModel->item_request($item);
		$this->load->view('head');
		$this->load->view('navbar');
		$this->load->view('item', $result);
		$this->load->view('footer');
		$this->load->view('req_js');
	}

	public function logincheck()
	{

		$this->load->model('AccountsModel');
		$this->AccountsModel->check_login($_POST);
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url('welcome/index'), 'refresh');
	}

	public function signup()
	{
		$this->load->view('head');
		$this->load->view('navbar');
		$this->load->view('signup');
		$this->load->view('footer');
		$this->load->view('req_js');
	}

	public function signup_send()
	{
		$this->load->model('AccountsModel');
		$this->AccountsModel->insert_account($_POST);
	}

	public function account(){
		$this->load->view('head');
		$this->load->view('navbar');
		$this->load->model('AccountsModel');
		$data['result'] = $this->AccountsModel->get_account($_SESSION['accountID']);
		$this->load->model('TransactionsModel');
		$data['transaction'] = $this->TransactionsModel->get_transanctionsdelivered($_SESSION['accountID']);
		$data['transactionun'] = $this->TransactionsModel->get_transanctionsundelivered($_SESSION['accountID']);
		$this->load->view('account', $data);
		$this->load->view('footer');
		$this->load->view('req_js');
	}

	//Getting line items for account
	public function line_items(){
		if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST["id"])){
			$this->load->model('TransactionsModel');
			$result['line_items'] = $this->TransactionsModel->get_line_items($_POST['id']);
			echo json_encode($result['line_items']);
		}	
	}

	public function delete_transaction(){
		if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST["id"])){
			$this->load->model('TransactionsModel');
			$this->TransactionsModel->delete_transaction($_POST['id']);
		}	
	}

	public function account_updatepersonal(){
		$this->load->model('AccountsModel');
		$this->AccountsModel->update_personal($_POST);
	}

	public function account_updateemail(){
		$this->load->model('AccountsModel');
		$this->AccountsModel->update_email($_POST);
	}
	public function account_updatepassword(){
		$this->load->model('AccountsModel');
		$this->AccountsModel->update_password($_POST);
	}

	public function cart(){
		$this->load->view('head');
		$this->load->view('navbar');
		$this->load->library("cart");
		$data['items'] = $this->cart->contents();
		$data['cart_total'] = $this->cart->total();
		$this->load->view('cart', $data);
		$this->load->view('footer');
		$this->load->view('req_js');
		
	}
	public function cart_add(){
		if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST["id"])){
			$this->load->library("cart");
			$id = $_POST['id'];
			$name = $_POST['name'];
			//$name = str_replace("'",",",$string);
			$price = $_POST['price'];
			$item_array = array(  
			                     'id'             =>     $id,  
			                     'name'           =>     $name,  
			                     'price'          =>     $price,  
			                     'qty'       	  =>     1 						
			             );
			$this->cart->insert($item_array);
			$string = array('text' => /*$item_array['name']*/"1 item has been added to cart");
			echo json_encode($string);
		}else{
			$string = array('text' => "error");
			echo json_encode($string);
		}
	}

	public function cart_remove(){
		if($_SERVER['REQUEST_METHOD'] == "POST"){
			$this->load->library("cart");
		    $row_id = $_POST["row_id"];
		    $data = array(
		     'rowid'  => $row_id,
		     'qty'  => 0
		    );
		    $this->cart->update($data);
		    $array['total'] = number_format($this->cart->total(), 2);
		    echo json_encode($array);
		}else{
			$string = array('text' => "error");
			echo json_encode($string);
		}
	}

	public function cart_updateqty(){
		if($_SERVER['REQUEST_METHOD'] == "POST"){
			$this->load->library("cart");
		    $qty = $_POST["qty"];
		    $row_id = $_POST["row_id"];
		    $data = array(
		     'rowid'  => $row_id,
		     'qty'  => $qty
		    );
		    $this->cart->update($data);
		    $array = $this->cart->get_item($row_id);
		    $array['subtotal'] = number_format($array['subtotal'], 2);
		    $array['total'] = number_format($this->cart->total(), 2);
		    echo json_encode($array);
		}else{
			$string = array('text' => "error");
			echo json_encode($string);
		}
	}

	public function cart_clear(){
		$this->load->library("cart");
  		$this->cart->destroy();
	}

	public function get_address(){
		if($_SERVER['REQUEST_METHOD'] == "POST"){
			$this->load->model('AccountsModel');
			$data['result'] =  $this->AccountsModel->get_account($_SESSION['accountID']);
			echo json_encode($data['result']);
		}
	}

	public function transaction_add(){
		if($_SERVER['REQUEST_METHOD'] == "POST"){
			$this->load->library("cart");
			$total = $this->cart->total();
			$this->load->model('TransactionsModel');
			$trans_ID = $this->TransactionsModel->insertTransaction($total, $_POST['payment']);
			foreach($this->cart->contents() as $items){
				$this->TransactionsModel->insertLineitems($trans_ID, $items['id'], $items['qty']);
			}
			$this->cart->destroy();
		}
		
	}


	public function index_products()
	{
		$this->load->model("ProductsModel");
		$data['popular'] = $this->ProductsModel->get_popular_products();
		$data['new'] = $this->ProductsModel->get_new_products();
		$this->load->view('index',$data);
	}

	public function products_display(){
		if($_SERVER['REQUEST_METHOD'] == "POST"){
			if($_POST['type'] == "popular"){
				$this->load->model('ProductsModel');
				$result['data'] = $this->ProductsModel->get_popular_products();
				echo json_encode($result['data']);
			}else if($_POST['type'] == "new"){
				$this->load->model('ProductsModel');
				$result['data'] = $this->ProductsModel->get_new_products();
				echo json_encode($result['data']);
			}
		}
	}

	public function products_request(){
		if($_SERVER['REQUEST_METHOD'] == "POST"){
			$this->load->model('ProductsModel');
			$result['data'] = $this->ProductsModel->get_entries($_POST['type'], $_POST['category']);
			echo json_encode($result['data']);
		}
	}

	public function products_search(){
		if($_SERVER['REQUEST_METHOD'] == "POST"){
			$this->load->model('ProductsModel');
			$result['data'] = $this->ProductsModel->products_search($_POST['query']);
			echo json_encode($result['data']);
		}
	}

	public function searchresults(){
		if($_SERVER['REQUEST_METHOD'] == "POST"){
			$this->load->view('head');
			$this->load->view('navbar');
			$data['input'] = $_POST['input'];
			$this->load->view('searchresults', $data);
			$this->load->view('footer');
		}
	}

	public function employee()
	{
		$this->load->model("TransactionsModel");
		$data["chart1"] = $this->TransactionsModel->chart1data();
		$data["chart2"] = $this->TransactionsModel->chart2data();
		$data["chart3"] = $this->TransactionsModel->chart3data();
		$this->load->view('head');
		$this->load->view('navbar');
		$this->load->view('employee',$data);

	}

	public function employee_products()
	{
	
		$this->load->model("ProductsModel");
		$data["productlist"] = $this->ProductsModel->get_all_products();
		$this->load->view('head');
		$this->load->view('navbar');
		$this->load->view("employee_products",$data);
	}

	public function employee_customers()
	{
		$this->load->model("AccountsModel");

		$data["customerlist"] = $this->AccountsModel->get_all_active_customers();
		$data["inactive"] = $this->AccountsModel->get_all_inactive();
		$data["employeelist"] = $this->AccountsModel->get_all_employees();
		$data["couriers"] = $this->AccountsModel->get_all_couriers();
		$this->load->view('head');
		$this->load->view('navbar');
		$this->load->view("employee_customers",$data);
	}
	

	public function insert_product(){
		if(isset($_POST['product_name'])){

			$this->load->model("ProductsModel");
			$this->ProductsModel->add_product($_POST['product_name'],$_POST['product_desc'],$_POST['product_type'],$_POST['product_price'],$_POST['product_stock']);
			$data["productlist"] = $this->ProductsModel->get_all_products();
		}
			$this->load->view('head');
			$this->load->view('navbar');
			$this->load->view('employee_products',$data);
		
	}

	public function delete_product(){
		if($_SERVER["REQUEST_METHOD"] == "POST"){

		$this->load->model("ProductsModel");
		$data["productlist"] = $this->ProductsModel->get_all_products();
			
			$this->ProductsModel->remove_product($_POST['id']);
			$this->load->view('head');
			$this->load->view('navbar');
			$this->load->view('employee_products',$data);
			
		}
		
	}
	public function add_stocks(){
		if(isset($_POST['amount'])){
			$this->load->model("ProductsModel");
			$this->ProductsModel->add_stocks($_POST['pID'],$_POST['amount'],$_POST['operation']);
		}
			$data["productlist"] = $this->ProductsModel->get_all_products();
			$this->load->view('head');
			$this->load->view('navbar');
			$this->load->view('employee_products',$data);
		}
	public function change_customer_type(){
		if(isset($_POST['uid'])){
			$this->load->model("AccountsModel");
			$this->AccountsModel->change_type($_POST['uid'],$_POST['type']);
		}

		$data["customerlist"] = $this->AccountsModel->get_all_active_customers();
		$this->load->view('head');
		$this->load->view('navbar');
		$this->load->view('employee_customers',$data);

		}
	public function make_inactive(){
		
			$this->load->model('AccountsModel');
			$this->AccountsModel->set_inactive($_POST['id']);
		
		$this->load->view('head');
		$this->load->view('navbar');
		$this->load->view('employee_customers',$data);

	}
	public function make_active(){
		
			$this->load->model('AccountsModel');
			$this->AccountsModel->set_active($_POST['id']);
		
		$this->load->view('head');
		$this->load->view('navbar');
		$this->load->view('employee_customers',$data);

	}
	
	public function courier(){
		$this->load->view('head');
		$this->load->view('navbar');
		$this->load->model('TransactionsModel');
		$data['unapproved'] = $this->TransactionsModel->get_unapproved_deliveries();
		$data['undelivered'] = $this->TransactionsModel->get_undelivered();
		$data['delivered'] = $this->TransactionsModel->get_delivered();
		$this->load->view('courier_delivery',$data);
	}
	public function set_delivery(){
		$this->load->model("TransactionsModel");
		$this->TransactionsModel->delivered($_POST['id'],$_POST['courier']);
		$this->load->view('head');
		$this->load->view('navbar');
		$data['undelivered'] = $this->TransactionsModel->get_undelivered();
		$this->load->view('courier_delivery',$data);
	}
	public function delete_delivery(){
			$this->load->model("TransactionsModel");
		$this->TransactionsModel->del_delivery($_POST['id']);
		$this->load->view('head');
		$this->load->view('navbar');
		$data['undelivered'] = $this->TransactionsModel->get_undelivered();
		$this->load->view('courier_delivery',$data);
	}
	public function employee_transactions(){
		$this->load->model("TransactionsModel");
		$this->load->view('head');
		$this->load->view('navbar');
		$data['trans'] = $this->TransactionsModel->get_delivered();
		$data['unapproved'] = $this->TransactionsModel->get_unapproved_deliveries();
		$this->load->view('employee_transactions',$data);
	}
	public function set_approved(){
		$this->load->model("TransactionsModel");
		$this->TransactionsModel->emp_approve($_POST['id']);
		$this->load->view('head');
		$this->load->view('navbar');
		$data['trans'] = $this->TransactionsModel->get_transactions_isdelivered();
		$data['unapproved'] = $this->TransactionsModel->get_unapproved_deliveries();
		$this->load->view('employee_transactions',$data);
	}
	public function set_disapproved(){
		$this->load->model("TransactionsModel");
		$this->TransactionsModel->emp_disapprove($_POST['id']);
		$this->load->view('head');
		$this->load->view('navbar');
		$data['trans'] = $this->TransactionsModel->get_transactions_isdelivered();
		$data['unapproved'] = $this->TransactionsModel->get_unapproved_deliveries();
		$this->load->view('employee_transactions',$data);
	}



}
