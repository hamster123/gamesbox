<?php
	$one = "SELECT product_type,COUNT(*) AS x
		   FROM line_item l
		   JOIN product p 
		   ON p.product_ID = l.product_ID
		   GROUP BY p.product_type
		   ORDER BY x DESC";
	//Percentage of product types  on all transactions
	
	$two = "SELECT person_type,COUNT(*) AS x
				FROM person
				GROUP BY person_type
				ORDER BY x DESC";
	//Composition of person classifications
	
	$thr = "SELECT customer_ID,ROUND(AVG(transaction_total),2)x
			FROM transaction t
			GROUP BY customer_ID
			ORDER BY x DESC
			LIMIT 7";
	//Top 7 Customers with highest total transactions
	
	
	$fou = "SELECT MONTHNAME(delivery_date)month,COUNT(*)num
			FROM delivery
			GROUP BY month
			ORDER BY num DESC";
	//Months with highest numbers  of deliveries
	
	$products = "SELECT *
				FROM product
				ORDER BY product_name";
	
	$person = "SELECT *
			FROM person
			WHERE person_type = 'customer'
			ORDER BY person_lname";
	
?>