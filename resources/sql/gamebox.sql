-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 22, 2018 at 05:21 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `gamebox`
--

-- --------------------------------------------------------

--
-- Table structure for table `delivery`
--

CREATE TABLE IF NOT EXISTS `delivery` (
`delivery_id` int(11) NOT NULL,
  `delivery_request_date` datetime NOT NULL,
  `delivery_finish` datetime NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `isDelivered` tinyint(1) NOT NULL DEFAULT '0',
  `delivered_by` int(11) DEFAULT NULL,
  `employee_approved` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `delivery`
--

INSERT INTO `delivery` (`delivery_id`, `delivery_request_date`, `delivery_finish`, `transaction_id`, `isDelivered`, `delivered_by`, `employee_approved`) VALUES
(31, '2018-03-22 16:20:15', '2018-03-22 22:16:17', 1, 1, 12, 1),
(32, '2018-03-22 16:20:50', '0000-00-00 00:00:00', 2, 0, NULL, 1),
(33, '2018-03-22 16:21:02', '0000-00-00 00:00:00', 3, 0, NULL, 1),
(34, '2018-03-22 16:21:59', '0000-00-00 00:00:00', 4, 0, NULL, 1),
(35, '2018-03-22 16:22:36', '0000-00-00 00:00:00', 5, 0, NULL, 1),
(36, '2018-03-22 16:23:41', '0000-00-00 00:00:00', 6, 0, NULL, 1),
(38, '2018-03-22 16:24:57', '0000-00-00 00:00:00', 8, 0, NULL, 1),
(39, '2018-03-22 16:25:28', '0000-00-00 00:00:00', 9, 0, NULL, 1),
(40, '2018-03-22 16:26:23', '0000-00-00 00:00:00', 10, 0, NULL, 1),
(42, '2018-03-22 16:27:15', '0000-00-00 00:00:00', 12, 0, NULL, 1),
(43, '2018-03-22 16:27:24', '0000-00-00 00:00:00', 13, 0, NULL, 0),
(44, '2018-03-22 16:27:41', '0000-00-00 00:00:00', 14, 0, NULL, 0),
(45, '2018-03-23 00:02:48', '0000-00-00 00:00:00', 15, 0, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `line_item`
--

CREATE TABLE IF NOT EXISTS `line_item` (
`line_item_ID` int(9) NOT NULL,
  `transaction_ID` int(9) NOT NULL,
  `product_ID` int(9) NOT NULL,
  `qty` int(9) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=141 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `line_item`
--

INSERT INTO `line_item` (`line_item_ID`, `transaction_ID`, `product_ID`, `qty`) VALUES
(102, 1, 5, 1),
(103, 1, 13, 1),
(104, 1, 18, 1),
(105, 1, 23, 1),
(106, 2, 19, 1),
(107, 3, 6, 1),
(108, 3, 7, 1),
(109, 3, 10, 1),
(110, 4, 13, 1),
(111, 4, 18, 1),
(112, 5, 3, 1),
(113, 6, 19, 1),
(114, 6, 3, 1),
(115, 6, 5, 1),
(116, 7, 21, 1),
(117, 7, 18, 1),
(118, 7, 19, 1),
(119, 8, 21, 4),
(120, 8, 7, 1),
(121, 8, 5, 4),
(122, 8, 17, 4),
(123, 9, 4, 1),
(124, 10, 16, 4),
(125, 10, 15, 1),
(126, 10, 14, 4),
(127, 10, 17, 1),
(128, 10, 12, 17),
(129, 11, 19, 1),
(130, 11, 20, 1),
(131, 11, 21, 1),
(132, 12, 17, 1),
(133, 12, 16, 1),
(134, 12, 14, 1),
(135, 13, 2, 1),
(136, 14, 19, 1),
(137, 14, 15, 10),
(138, 15, 18, 1),
(139, 15, 17, 1),
(140, 15, 14, 1);

-- --------------------------------------------------------

--
-- Table structure for table `person`
--

CREATE TABLE IF NOT EXISTS `person` (
`person_ID` int(9) NOT NULL,
  `person_fname` varchar(30) NOT NULL,
  `person_lname` varchar(30) NOT NULL,
  `person_phoneNum` varchar(11) NOT NULL,
  `person_address` varchar(50) NOT NULL,
  `person_type` enum('employee','customer','admin','courier') NOT NULL,
  `person_password` varchar(15) NOT NULL,
  `person_email` varchar(50) NOT NULL,
  `date_joined` datetime NOT NULL,
  `isActive` tinyint(1) NOT NULL DEFAULT '1',
  `city` varchar(20) NOT NULL,
  `zip_code` int(6) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `person`
--

INSERT INTO `person` (`person_ID`, `person_fname`, `person_lname`, `person_phoneNum`, `person_address`, `person_type`, `person_password`, `person_email`, `date_joined`, `isActive`, `city`, `zip_code`) VALUES
(1, 'Electro', 'Bozman', '09112414241', 'Joan Romero 666-4366 Lacinia Avenue Idaho Falls Oh', 'courier', 'woomwoomwoom', 'ElectricFan1@tomhardware.com', '2018-03-11 00:00:00', 1, 'cebu', 6018),
(2, 'Will', 'Smith', '989890098', 'sampleaddress', 'employee', 'blackboi99', 'willsmith@gmail.com', '2018-03-11 00:00:00', 1, 'cebu', 6018),
(3, 'Jones', 'Jodama', '09465522552', 'j', 'customer', '12341234', 'jonesolutions@gmail.com', '2018-03-11 00:00:00', 0, 'cebu', 6018),
(4, 'Irvin', 'Baldovino', '5318008', 'Cara Whitehead 4005 Praesent St. Torrance Wyoming ', 'courier', 'newpass', 'birvin@gmail.com', '2018-03-08 00:00:00', 1, 'cebu', 6018),
(6, 'Broski', 'Cornbar', '09458567777', 'Davis Patrick P.O. Box 147 2546 Sociosqu Rd. Bethl', 'customer', 'testing', 'christian@gmail.com', '2018-03-14 00:00:00', 1, 'cebu', 6018),
(8, 'Mandy', 'Moran', '0945658754', 'Kyla Olsen Ap #651-8679 Sodales Av. Tamuning PA 10', 'customer', 'newpass', 'MMoran@gmail.com', '2018-03-07 00:00:00', 0, 'cebu', 6018),
(9, 'John', 'Strauss', '09456258752', 'Forrest Ray 191-103 Integer Rd.', 'customer', 'newpass', 'JStrauss', '2018-01-11 00:00:00', 1, 'cebu', 6018),
(10, 'Michael', 'Brown', '09458752365', 'Hiroko Potter P.O. Box 887 2508 Dolor. Av. Muskego', 'courier', 'newpass', 'Mbrown@sample.com', '2018-01-12 00:00:00', 0, 'Mandaue', 6018),
(11, 'Sean', 'Lee', '09453654285', 'Nyssa Vazquez 511-5762 At Rd. Chelsea MI 67708', 'customer', 'newpass', 'SLee@sample.com', '2018-02-22 00:00:00', 1, 'cebu', 6018),
(12, 'James', 'Connor', '09458745632', 'Lawrence Moreno 935-9940 Tortor. Street Santa Rosa', 'courier', 'imacourier', 'OJames@sample.com', '2017-09-22 00:00:00', 1, 'cebu', 6018),
(13, 'Sandra', 'Mendoza', '09456854236', 'Ina Moran P.O. Box 929 4189 Nunc Road Lebanon KY 6', 'customer', 'customergirl', 'SMendoza@sample.com', '2018-01-26 00:00:00', 1, 'cebu', 6018),
(14, 'Carina', 'Bayron', '0995555142', 'Aaron Hawkins 5587 Nunc. Avenue Erie Rhode Island ', 'customer', '054125', '9carina@gmail.com', '2018-01-26 00:00:00', 1, 'cebu', 6018),
(15, 'Johnny', 'Johnson', '433589657', 'Hedy Greene Ap #696-3279 Viverra. Avenue Latrobe D', 'customer', '051656', 'JJoriginal@sample.com', '2018-01-26 00:00:00', 1, 'cebu', 6018),
(16, 'Mark', 'Casona', '0945742563', 'Melvin Porter P.O. Box 132 1599 Curabitur Rd. Band', 'courier', 'manamamark', 'MCasona@gmail.com', '2018-01-29 00:00:00', 1, 'cebu', 6018),
(17, 'John Clarence', 'Del Mar', '09959349305', '269 S.B Cabahug Street, Mandaue City', 'employee', 'jcdelmar', 'johndelmar7@gmail.com', '2018-03-08 00:00:00', 1, 'cebu', 6018),
(18, 'Bryle', 'Baran', '09082152321', 'Leilani Boyer 557-6308 Lacinia Road San Bernardino', 'employee', '9bryne', 'brylebaran@gmail.com', '2018-03-08 00:00:00', 1, 'cebu', 6018),
(19, 'Carl', 'Merban', '09458754213', 'Colby Bernard Ap #285-7193 Ullamcorper Avenue Ames', 'customer', 'samplepass', 'Cmerboy@gmail.com', '2018-03-09 00:00:00', 1, 'cebu', 6018),
(20, 'Sophie', 'Turner', '09457854125', 'Bryar Pitts 5543 Aliquet St. Fort Dodge GA 20783', 'customer', 'sophie1', 'tsophie@gmail.com', '2018-03-08 00:00:00', 1, 'cebu', 6018),
(21, 'Jonathan', 'Hughes', '09458754254', 'Rahim Henderson 5037 Diam Rd. Daly City Ohio 90255', 'customer', 'manamajohn', 'jhughes@gmail.com', '2018-03-09 00:00:00', 1, 'cebu', 6018),
(22, 'Alphonse', 'Elric', '09458742153', 'Noelle Adams 6351 Fringilla Avenue Gardena Colorad', 'customer', 'japrobocop', 'ealphonse@sample.com', '2018-03-02 00:00:00', 1, 'cebu', 6018),
(23, 'Miyuki', 'Kujou', '09457584521', 'Lillith Daniel 935-1670 Neque. St. Centennial Dela', 'customer', 'bestgrill', 'mkujou@gmail.com', '2018-03-03 00:00:00', 1, 'cebu', 6018),
(24, 'Zach', 'Pars', '09157775831', '69', 'customer', 'Lordmassten12', 'unsuspectinglemon@gmail.com', '2018-03-21 00:00:00', 1, 'Cebu', 6000),
(25, 'John', 'delMar', '09959439293', '269 SB Cabahug Street Mandaue City', 'customer', 'jcdelmar', 'johndelmar8@sample.com', '2018-03-21 00:00:00', 1, 'Mandaue', 9018),
(26, 'Sire', 'Lance', '232423432', 'Mabolo, f. gochan', 'customer', '123123123', 'bryne@gmail.com', '2018-03-21 00:00:00', 1, 'Cebu', 6000),
(27, 'Bryle', 'Baran', '09925953535', '269 SB Cabahug Street Mandaue City', 'customer', 'jcdelmar', 'sampleuser@sample.com', '2018-03-21 00:00:00', 1, 'Mandaue', 6018),
(28, 'test', 'test', '09999999999', 'test address', 'customer', 'testpassword', 'testcustomer@test.com', '2018-03-22 22:41:48', 1, 'test city', 6018),
(29, 'test', 'test', '09999999999', 'test address', 'courier', 'testpassword', 'testcourier@test.com', '2018-03-22 22:42:38', 1, 'test city', 6018),
(30, 'test', 'test', '0999999999', 'test address', 'employee', 'testpassword', 'testemployee@test.com', '2018-03-22 22:43:08', 1, 'test city', 6019);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
`product_ID` int(9) NOT NULL,
  `product_name` varchar(50) NOT NULL,
  `product_desc` varchar(100) NOT NULL,
  `product_price` double(9,2) NOT NULL,
  `product_type` enum('Video Game','Hardware','other') NOT NULL,
  `product_stock` int(12) NOT NULL,
  `date_added` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_ID`, `product_name`, `product_desc`, `product_price`, `product_type`, `product_stock`, `date_added`) VALUES
(2, 'Assasin''s Creed Unity', ' An action-adventure video game.', 2100.00, 'Video Game', 8, '2018-03-11'),
(3, 'Fallout 4', 'An action role-playing video game.', 2300.00, 'Video Game', 5, '2018-03-11'),
(4, 'Blackwidow Chroma 2017 ', 'Mechanical Keyboard', 7600.50, 'Hardware', 2, '2018-03-11'),
(5, 'CDR-King Black', 'Gaming Mouse', 650.00, 'Hardware', 2, '2018-03-11'),
(6, 'Insurgency', 'a multiplayer first-person shooter video game', 1500.00, 'Video Game', 2, '2018-03-11'),
(7, 'Shower with your dad simulator 2015', 'a fast paced shower-simulation where you shower with your 8-bit dad.', 2300.00, 'Video Game', 2, '2018-03-11'),
(8, 'G402 Hyperion Fury', 'Gaming Mouse from Logitech', 2700.00, 'Hardware', 2, '2018-03-11'),
(9, 'G502 Proteus Spectrum', 'Gaming Mouse from Logitech', 5200.00, 'Hardware', 2, '2018-03-11'),
(10, 'Counter Strike Global Offensive', 'a multiplayer first-person shooter video game developed by Hidden Path Entertainment and Valve Corp.', 419.50, 'Video Game', 2, '2018-03-11'),
(11, 'Sid Meier''s Civilization V', 'Sid Meier''s Civilization V is a 4X video game in the Civilization series developed by Firaxis Games.', 2400.00, 'Video Game', 2, '2018-03-11'),
(12, 'Call of Duty  World at War', 'The 900th CoD game.', 1050.80, 'Video Game', 2, '2018-03-11'),
(13, 'Shrek 2', 'best movie', 1999.99, 'other', 2, '2018-03-11'),
(14, 'Shrek 3', 'best movie sequel', 2999.99, 'other', 2, '2018-03-11'),
(15, 'Hyper X Cloud 2', 'Gaming Headset', 2499.00, 'Hardware', 2, '2018-03-11'),
(16, 'Razer Kraken Chroma 2016 Edition', 'RGB Gaming Headset', 4050.00, '', 2, '2018-03-11'),
(17, 'Player Unknown''s BattleGrounds', 'PUBG is a multiplayer online battle royale video game.', 749.50, 'Video Game', 2, '2018-03-11'),
(18, 'Fortnite', 'a PUBG ripoff.', 200.00, 'Video Game', 2, '2018-03-11'),
(19, 'H1Z1  King of the kill', 'discount PUBG', 419.50, 'Video Game', 2, '2018-03-11'),
(20, 'Logitech Daedalus Prime', 'Gaming mouse from Logitech', 1499.99, 'Hardware', 2, '2018-03-11'),
(21, 'Huniepop', ' is a unique sim experience for PC, Mac and Linux.', 500.00, 'Video Game', 2, '2018-03-11'),
(23, 'Pop Socket', 'pop socket', 350.00, 'other', 56, '2018-03-21');

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE IF NOT EXISTS `transaction` (
`transaction_ID` int(9) NOT NULL,
  `transaction_Date` datetime NOT NULL,
  `customer_ID` int(9) NOT NULL,
  `transaction_total` double(11,2) NOT NULL,
  `payment` enum('on_delivery','with_card') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaction`
--

INSERT INTO `transaction` (`transaction_ID`, `transaction_Date`, `customer_ID`, `transaction_total`, `payment`) VALUES
(1, '2018-03-22 16:20:15', 3, 3199.99, 'on_delivery'),
(2, '2018-03-22 16:20:49', 3, 419.50, 'with_card'),
(3, '2018-03-22 16:21:02', 3, 4219.50, 'with_card'),
(4, '2018-03-22 16:21:59', 3, 2199.99, 'with_card'),
(5, '2018-03-22 16:22:36', 3, 2300.00, 'with_card'),
(6, '2018-03-22 16:23:41', 13, 3369.50, 'on_delivery'),
(7, '2018-03-22 16:23:52', 13, 1119.50, 'on_delivery'),
(8, '2018-03-22 16:24:57', 14, 9898.00, 'on_delivery'),
(9, '2018-03-22 16:25:28', 14, 7600.50, 'on_delivery'),
(10, '2018-03-22 16:26:23', 14, 49312.06, 'on_delivery'),
(11, '2018-03-22 16:26:56', 16, 2419.49, 'on_delivery'),
(12, '2018-03-22 16:27:15', 16, 7799.49, 'on_delivery'),
(13, '2018-03-22 16:27:24', 16, 2100.00, 'on_delivery'),
(14, '2018-03-22 16:27:41', 16, 25409.50, 'on_delivery'),
(15, '2018-03-23 00:02:48', 27, 3949.49, 'on_delivery');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `delivery`
--
ALTER TABLE `delivery`
 ADD PRIMARY KEY (`delivery_id`), ADD KEY `fk_transaction_id` (`transaction_id`);

--
-- Indexes for table `line_item`
--
ALTER TABLE `line_item`
 ADD PRIMARY KEY (`line_item_ID`);

--
-- Indexes for table `person`
--
ALTER TABLE `person`
 ADD PRIMARY KEY (`person_ID`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
 ADD PRIMARY KEY (`product_ID`);

--
-- Indexes for table `transaction`
--
ALTER TABLE `transaction`
 ADD PRIMARY KEY (`transaction_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `delivery`
--
ALTER TABLE `delivery`
MODIFY `delivery_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `line_item`
--
ALTER TABLE `line_item`
MODIFY `line_item_ID` int(9) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=141;
--
-- AUTO_INCREMENT for table `person`
--
ALTER TABLE `person`
MODIFY `person_ID` int(9) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
MODIFY `product_ID` int(9) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `transaction`
--
ALTER TABLE `transaction`
MODIFY `transaction_ID` int(9) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `delivery`
--
ALTER TABLE `delivery`
ADD CONSTRAINT `fk_transaction_id` FOREIGN KEY (`transaction_id`) REFERENCES `transaction` (`transaction_ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
